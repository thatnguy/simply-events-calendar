<?php
/**
 * Admin hooks are define here.
 *
 * @package  Simply Events
 */
class Simply_Events_Admin {


	/**
	 * Error messages that are displayed in admin_notices hook.
	 * These errors are indexed by a unique code.
	 */
	private $errors;

	private $plugin_name;

	private $version;

	private $options_name;

	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->options_name = "{$this->plugin_name}_options";
		
		$this->setup_error_notices();

	}

	/**
	 * set admin error notices using query params.
	 */
	private function setup_error_notices() {
		$this->errors = array(
			// meta box errors.
			1 => 'Event start date, end date inputs required.',
			2 => 'An invalid event start time or end time found. Please re-input the event times correctly.',
			3 => 'The setted start time has passed.',
			4 => 'End time can not be behind start time.',
			5 => 'Invalid latitude or longitude coordinates for the inputted location.'
		);

		// admin_notices hook for our errors
		add_action( 'admin_notices', array( $this, 'error_messages' ) );
	}

	/**
	 * Register options, option sections and fields.
	 */
	private function register_options() {

		// register our setting.
		register_setting( $this->options_name, $this->options_name, array( $this, 'validate_options' ) );

		// register setting sections.
		$section_name = "{$this->options_name}_main";
		add_settings_section( $section_name, 'Main Settings', function() {}, $this->plugin_name );

		// register setting fields.

		// use google map field
		$field_id = "{$this->plugin_name}-google-map";
		add_settings_field( $field_id, 'Use Google Map', function( $args ) {
			$options = get_option( $this->options_name );
			printf( '<input id="%s" name="%s" type="checkbox" value="1" %s/>',
				$args['id'],
				"{$this->options_name}[use_google_map]",
				checked( isset( $options['use_google_map'] ), true, false )
			);

		}, $this->plugin_name, $section_name, array( 'id' => $field_id ) );

		// google map api key field.
		$field_id = "{$this->plugin_name}-google-map-key";
		add_settings_field( $field_id, 'Google Map API Key', function( $args ) {
			$options = get_option( $this->options_name );

			printf( '<input id="%s" name="%s" type="text" size="60" value="%s" />',
				$args['id'],
				"{$this->options_name}[google_map_key]",
				$options['google_map_key']
			);

		}, $this->plugin_name, $section_name, array( 'id' => $field_id ) );

		// @todo
		// alert event field.
	}

	/**
	 * Admin init hook.
	 */
	public function admin_init() {
		$this->register_options();
	}

	/**
	 * Post Edit screen (admin area)
	 */
	public function admin_edit_screen_load() {
		add_filter( 'request', array( $this, 'event_sort_column' ) );

		// handling filtering of events in the edit.php screen.
		add_action( 'pre_get_posts', array( $this, 'event_list_filter' ) );
	}

	/**
	 * Register the stylesheets for the admin area.
	 */
	public function enqueue_styles() {
		$script_uri = plugin_dir_url( __FILE__ ) . 'css';

		// enqueue our main style.
		wp_enqueue_style( $this->plugin_name, "{$script_uri}/simply-events.css", array(), $this->version, 'all' );
		
	}

	/**
	 * Register the JavaScript for the admin area.
	 */
	public function enqueue_scripts() {

		$script_uri = plugin_dir_url( __FILE__ ) . 'js';

		// enqueue the jquery datepicker.
		$this->enqueue_datepicker();

		// enqueue main script.
		wp_enqueue_script( $this->plugin_name, "{$script_uri}/simply-events.js", array( 'jquery' ), $this->version, true );

		// enqeueue google maps
		$this->enqueue_google_maps();

	}

	public function enqueue_datepicker() {
		// enqueue jQuery UI datepicker.
		// @note there are no css for datepicker we use:
		// theme for datepicker http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/themes/base/jquery-ui.css
		wp_enqueue_style( 'jquery-ui-datepicker', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/themes/base/jquery-ui.css' );

		// enqueue jQuery UI datepicker.
		wp_enqueue_script( 'jquery-ui-datepicker' );
	}

	/**
	 * Enqueue the google map script if google map api key is set
	 */
	public function enqueue_google_maps() {
		
		// check if options is set to use google maps and there is a google map api key set.
		$api_key = $this->using_google_map();
		if ( $api_key ) {
			wp_enqueue_script( 
				'google-map',
				"https://maps.googleapis.com/maps/api/js?key={$api_key}&libraries=places&callback=googleMapInit",
				array(),
				'',
				true 
			);
		}
	}

	/**
	 * Add async or|and defer to scripts
	 * @param string $tag     current script tag
	 * @param string $handler current script handler
	 */
	public function add_async_defer_scripts( $tag, $handler ) {
		$scripts = array(
			// handler
			'google-map' => array(
				'async' => true,
				'defer' => true,
			)
		);

		if ( isset( $scripts[ $handler ] ) ) {
			$methods = $scripts[ $handler ];

			if ( isset( $methods['async'] ) && $methods['async'] ) {
				$tag = str_replace( ' src', ' async src', $tag );
			}

			if ( isset( $methods['defer'] ) && $methods['defer'] ) {
				$tag = str_replace( ' src', ' defer src', $tag );
			}
		}

		return $tag;
	}

	/**
	 * Register our custom post type for managing events.
	 * 
	 */
	public function register_event_post_type() {
		$args = array(
			'description'         => __( 'An event post type for events specific.', 'simply-events' ),
			'public'              => true,
			'publicly_queryable'  => true,
			'exclude_from_search' => false,
			'show_in_nav_menus'   => false,
			'show_ui'             => true,
			// show in admin menu. 'show_ui' must be true.
			'show_in_menu'        => true,
			'show_in_admin_bar'   => true,
			// the order of the post type should appear in the admin menu. 'show_in_menu' must be true.
			'menu_position'       => 4,	
			// defaults to post icon. @see https://developer.wordpress.org/resource/dashicons/
			'menu_icon'           => 'dashicons-calendar-alt', 
			// can posts of this post type be exported via import/export plugin.
			'can_export'          => true,	
			// delete post on author deletion.
			'delete_with_user'    => true,
			'hierarchical'        => false,
			'has_archive'         => true,
			'query_var'           => 'simply_events',
			// Whether Wordpress should map the meta capabilities (edit_post, read_post, delete_post) for you.
			'map_meta_cap' => true,
			// URL structure of this post type.
			'rewrite' => array(
				'slug'       => 'events',
				'with_front' => false,
				'pages'      => true,
				'feeds'      => true,
				'ep_mask'    => EP_PERMALINK,
			),
			// Wordpress features this post type supports. You can opt to set these later with add_post_type_support().
			'supports' => array(
				'title',			// $post->post_title
				'editor',			// $post->post_content
				// 'author',
				'thumbnail',  		// theme must support 'post-thumbnails'.
				// 'comments',  		// displays comments meta box
			),

			'labels' => array(
				'name'                  => __( 'Events', 'simply-events' ),
				'singular_name'         => __( 'Event', 'simply-events' ),
				'menu_name'             => __( 'Events', 'simply-events' ),
				'name_admin_bar'        => __( 'Events', 'simply-events' ),
				'add_new'               => __( 'Add New Event', 'simply-events' ),
				'add_new_item'          => __( 'Add New Event', 'simply-events' ),
				'edit_item'             => __( 'Edit Event', 'simply-events' ),
				'new_item'              => __( 'New Event', 'simply-events' ),
				'view_item'             => __( 'View Event', 'simply-events' ),
				'search_items'          => __( 'Search Events', 'simply-events' ),
				'not_found'             => __( 'No events found', 'simply-events' ),
				'not_found_in_trash'    => __( 'No events found in trash', 'simply-events' ),
				'all_items'             => __( 'All Events', 'simply-events' ),
				'insert_into_item'      => __( 'Insert into event', 'simply-events' ),
				'uploaded_to_this_item' => __( 'Uploaded to this event', 'simply-events' ),
				'views'                 => __( 'Filter events list', 'simply-events' ),
				'pagination'            => __( 'Events list navigation', 'simply-events' ),
				'list'                  => __( 'Events list', 'simply-events' ),
			),

			// WP RESTable.
			'show_in_rest' => true,

		);

		register_post_type( $this->plugin_name, $args );
	}

	/**
	 * Manage event column headers
	 * @param  Array $columns array of column headers => header name
	 * @return Array          column headers.
	 */
	public function manage_event_columns( $columns ) {
		$new_columns = array(
			'cb'             => $columns['cb'],
			'title'          => __( 'Event', 'simply-events' ),
			'start_datetime' => __( 'Start DateTime', 'simply-events' ),
			'end_datetime'   => __( 'End DateTime', 'simply-events' ),
			'location'       => __( 'Location', 'simply-events' ),
			'date'           => $columns['date'],
		);


 		return $new_columns;
	}

	/**
	 * Render the custom event column details.
	 * @param  string $column  current column name.
	 * @param  int $post_id current post id
	 */
	public function render_event_column( $column, $post_id ) {
		$key = "simply_events_{$column}";

		$value = get_post_meta( $post_id, $key, true );

		// just return if no meta value for column.
		if ( ! $value ) return ;

		switch ( $column ) {
			case 'start_datetime':
			case 'end_datetime'  :
				$time = new DateTime;
				$time->setTimestamp( $value );
				echo $time->format( 'D d F Y (g:i A)' );
				break;
			case 'location':
				echo $value;
				break;
			default:
				break;
		}
	}

	/**
	 * Add in sortable columns.
	 * @param  Array $columns an array of sortable columns.
	 * @return Array          array of sortable columns.
	 */
	public function event_sortable_columns( $columns ) {
		$columns['location']       = 'location';
		$columns['start_datetime'] = 'start_datetime';
		$columns['end_datetime']   = 'end_datetime';

		return $columns;
	}

	/**
	 * Sort event column
	 * @param  Array list of query variables
	 * @return Array list of query variables.
	 */
	public function event_sort_column( $vars ) {

		if ( isset( $vars['post_type'] ) && $this->plugin_name === $vars['post_type'] ) {
			if ( !empty( $vars['orderby'] ) ) {
				$sortables = array( 'location', 'start_datetime', 'end_datetime' );

				if ( in_array( $vars['orderby'], $sortables ) ) {

					$vars['meta_key'] = "simply_events_{$vars['orderby']}";
					$vars['orderby']  = 'meta_value';
				}
			}
		}

		return $vars;
	}

	/**
	 * add filtering events for the event list
	 * @param string $post_type current post type
	 */
	public function add_event_list_filters( $post_type = null ) {

		// WordPress version < 4.6 will not have these parameters passed.
		if ( is_null( $post_type ) ) {

			if ( ! isset( $_GET['post_type'] ) ) return;

			$post_type = $_GET['post_type'];
		}

		if ( $this->plugin_name !== $post_type ) return;

		$options = array(
			'Past Events' => 'past_events_filter',
			'Future Events' => 'future_events_filter',
		);

		$current = !empty( $_GET['event_list_filter'] ) ? $_GET['event_list_filter'] : '';
		?>
		<select name="event_list_filter">
			<option value=""><?php _e( 'Filter By', $this->plugin_name );?></option>
			<?php foreach ( $options as $lbl => $val ): ?>
			<option value="<?php echo $val;?>" <?php selected( $val, $current ); ?>><?php echo $lbl; ?></option>
			<?php endforeach; ?>
		</select>
		<?php
	}

	/**
	 * filter events based on the filter chosen
	 * @see   add_event_list_filters
	 * @param  WP_Query $query the current query object
	 * @return WP_Query   current query object.
	 */
	public function event_list_filter( $query ) {

		if ( !isset( $_GET['post_type'] ) || !isset( $_GET['event_list_filter'] ) ) return $query;

		$current = current_time( 'timestamp' );

		// default: future events
		$meta_query = array(
			'key' => 'simply_events_start_datetime',
			'value' => $current,
			'compare' => '>=',
		);

		if ( 'past_events_filter' === $_GET['event_list_filter'] ) {
			$meta_query['key'] = 'simply_events_end_datetime';
			$meta_query['compare'] = '<';
		}

		$query->set( 'meta_query', array( $meta_query ) );

		return $query;
	}

	/**
	 * setup metaboxes for our custom post type.
	 */
	public function setup_metaboxes() {

		// add meta boxes
		add_action( 'add_meta_boxes', array( $this, 'add_event_metaboxes' ), 10, 1 );

		// saving meta boxes data.
		add_action( 'save_post', array( $this, 'save_event_metaboxes' ), 10, 2 );

	}

	/**
	 * Add Event Metaboxes.
	 */
	public function add_event_metaboxes() {
		add_meta_box(
			'simply-event-details',
			__( 'Event Details', 'simply-events' ),
			array( $this, 'render_event_meta_content' ),
			'simply-events'
		);
	}


	/**
	 * Callback for rendering metabox content.
	 * @param  WP_Post $post current post
	 */
	public function render_event_meta_content( $post ) {
		wp_nonce_field( 'create_event_details', 'event_metabox_nonce' );

		$startdate = get_post_meta( $post->ID, 'simply_events_start_datetime', true );
		$enddate   = get_post_meta( $post->ID, 'simply_events_end_datetime', true );
		if ( $startdate && $enddate ) {
			$t = $startdate;
			$startdate = new DateTime;
			$startdate->setTimestamp( $t );

			$t = $enddate;
			$enddate = new DateTime;
			$enddate->setTimestamp( $t );
		}

		$location = get_post_meta( $post->ID, 'simply_events_location', true );
		$coords   = false;
		if ( $location ) $coords = get_post_meta( $post->ID, 'simply_events_coordinates', true );

		$link = get_post_meta( $post->ID, 'simply_events_link', true );

		$datetimes = array(
			'start' => array( 'name' => 'Start', 'datetime' => $startdate ),
			'end'   => array( 'name' => 'End', 'datetime' => $enddate ),
		);

		$r_freq = get_post_meta( $post->ID, 'simply_events_repeat_freq', true );
		$r_int   = get_post_meta( $post->ID, 'simply_events_repeat_interval', true );
		$r_data = get_post_meta( $post->ID, 'simply_events_repeat_data', true );
		?>
			<div class="se-wrapper">
				<div class="se-wrapper__datetime">
					<?php foreach ( $datetimes as $i => $value ): ?>
					<div class="datetime__<?php echo $i . 'time'; ?>">
						<div class="se-header"><?php echo "{$value['name']} date and time"; ?></div>
						<label class="se-label" for="se-<?php echo $i; ?>-hour">Time</label>
						<input class="se-input" id="se-<?php echo $i; ?>-hour" type="number" name="se-<?php echo $i; ?>-hour" placeholder="HH" value="<?php echo $value['datetime'] ? $value['datetime']->format( 'h' ) : ''; ?>" min="0" max="12" />
						<input class="se-input" type="number" name="se-<?php echo $i; ?>-min" placeholder="MM" value="<?php echo $value['datetime'] ? $value['datetime']->format( 'i' ) : '' ?>" min="0" max="59" />
						<select class="se-select-input" id="select-<?php echo $i; ?>-meridian" name="se-select-<?php echo $i; ?>-meridian">
						<?php 
							$meridian = null;
							if ( $value['datetime'] ) $meridian = $value['datetime']->format( 'A' );
						?>
							<option <?php echo is_null( $meridian ) || ( !is_null( $meridian ) && 'AM' === $meridian ) ? 'selected' : '';?>>AM</option>
							<option <?php echo !is_null( $meridian ) && 'PM' === $meridian ? 'selected' : ''; ?>>PM</option>
						</select>
						<label class="se-select-date">Date
						<input class="se-input" type="text" id="se-<?php echo $i; ?>-date" name="se-<?php echo $i; ?>-date" value="<?php echo $value['datetime'] ? $value['datetime']->format( 'F j, Y' ) : ''; ?>" placeholder="<?php echo "select {$i} date"; ?>" /></label>
					</div>
					<?php endforeach; ?>
					<div class="se-wrapper__date-repeat-wrapper">
						<label class="se-label"><input type="checkbox" id="se-repeat-btn" name="se-repeat" value="true" <?php echo $r_freq ? 'checked' : ''; ?>>Repeat</label>

						<div class="date-repeat-wrapper__wrap<?php echo $r_freq ? '' : ' hidden'; ?>">
						<?php 
							$r_freqs = array( 
								'daily'   => 'day', 
								'weekly'  => 'week',
								'monthly' => 'month',
								'yearly'  => 'year' 
							);

							$s = $r_freq && isset( $r_freqs[$r_freq] ) ? $r_freq : key( $r_freqs );
						?>
							<label class="se-label se-repeat-freq-label">Repeat
								<select id="se-repeat-frequency" name="se-repeat-frequency">
								<?php foreach ( $r_freqs as $k => $v ): ?>
									<?php $k_ucf = ucfirst( $k ); ?>
									<option value="<?php echo $k; ?>" title="Repeats <?php echo $k_ucf; ?>"<?php echo !is_null( $s ) && $k === $s ? ' selected' : ''; ?>><?php echo $k_ucf; ?></option>
								<?php endforeach; ?>
								</select>
							</label>
							<label class="se-label se-repeat-interval-label">Repeat every
								<select id="se-repeat-interval" name="se-repeat-interval">
								<?php $si = !$r_int ? 1 : (int) $r_int; ?>
								<?php for ( $i = 1; $i <= 30; ++$i ): ?>
									<option value="<?php echo $i; ?>"<?php echo $si === $i ? ' selected' : ''; ?> title="Repeats every <?php echo $i . ' ' . $r_freqs[$s] . ( 1 < $i ? 's' : '' ); ?>"><?php echo $i; ?></option>
								<?php endfor; ?>
								</select><span class="se-repeat-interval-text"><?php echo "{$r_freqs[$s]}s"; ?></span>
							</label>
							<div class="se-repeat-weekly-wrap<?php echo 'weekly' !== $s ? ' hidden' : '' ?>" data-toggle-ref="weekly"><span class="se-repeat-weekly-day-label">Repeat on:</span>
							<?php for ( $i = 0, $d = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'], $l = count( $d ); $i < $l; ++$i ): ?>
								<label class="se-label"><input type="checkbox" id="se-repeat-weekly-<?php echo $d[$i]; ?>" name="se-repeat-weekly-day[]" value="<?php echo $i; ?>" <?php echo !empty( $r_data['repeat_weekly_day'] ) && in_array( $i, $r_data['repeat_weekly_day'] ) ? 'checked' : ''; ?>><?php echo $d[$i]; ?></label>
							<?php endfor; ?>
							</div>

							<div class="se-repeat-monthly-wrap<?php echo 'monthly' !== $s ? ' hidden' : ''; ?>" data-toggle-ref="monthly">
								<label class="se-label" title="Repeat by day of the month">
									<input type="radio" id="se-repeat-monthly-dom" name="se-repeat-monthly-repeatby" value="dom"<?php echo !empty( $r_data['repeat_monthly_by'] ) && 'dom' === $r_data['repeat_monthly_by'] || !$r_data ? ' checked' : ''; ?>>
									day of the month
								</label>
								<label class="se-label" title="Repeat by day of the week">
									<input type="radio" id="se-repeat-monthly-dow" name="se-repeat-monthly-repeatby" value="dow"<?php echo !empty( $r_data['repeat_monthly_by'] ) && 'dow' === $r_data['repeat_monthly_by'] ? ' checked' : ''; ?>>
									day of the week
								</label>
							</div>
							<?php $has_end = !empty( $r_data['end'] ); ?>
							<div class="se-repeat-end-wrap">Ends:
								<label class="se-label" title="End never">
									<input type="radio" id="se-repeat-end-never" name="se-repeat-end" value="never"<?php echo $has_end && 'never' === $r_data['end'] || !$has_end ? ' checked' : ''; ?>><span class="se-repeat-choice-label">Never</span>
									</label>
								</label>
								<label class="se-label" title="End after number of set repeated occurrence">
									<input type="radio" id="se-repeat-end-occurrences" name="se-repeat-end" value="occurrence"<?php echo $has_end && 'occurrence' === $r_data['end'] ? ' checked' : ''; ?>><span class="se-repeat-choice-label">After</span> <input class="se-input" type="text" id="se-end-occurrence-amount" name="se-repeat-end-occurrence" value="<?php echo !empty( $r_data['end_occurrence'] ) ? $r_data['end_occurrence'] : ''; ?>" placeholder="number of occurrences">
								</label>
								<?php
									if ( !empty( $r_data['end_date'] ) ) {
										$r_data['end_date'] = ( new DateTime() )->setTimestamp( $r_data['end_date'] );
									}
								?>
								<label class="se-label" title="End after a specified date">
									<input type="radio" id="se-repeat-end-date" name="se-repeat-end" value="date"<?php echo $has_end && 'date' === $r_data['end'] ? ' checked' : '';?>><span class="se-repeat-choice-label">On</span> <input type="text" class="se-input" id="se-repeat-end-date-selection" name="se-repeat-end-date" value="<?php echo !empty( $r_data['end_date'] ) && $r_data['end_date'] instanceof DateTime ? $r_data['end_date']->format( 'F j, Y' ) : ''; ?>" placeholder="end date">
								</label>
							</div>

						</div>
					</div>
				</div>
				<div class="se-wrapper__location">
					<div class="se-header">Location</div>
					<input class="widefat" type="text" id="se-location" name="se-location" value="<?php echo $location ? $location : ''; ?>" placeholder="Please enter a valid address" />
					<input type="hidden" id="se-location-lat" name="se-location-lat" value="<?php echo $coords ? $coords['lat'] : '';?>" />
					<input type="hidden" id="se-location-lng" name="se-location-lng" value="<?php echo $coords ? $coords['lng'] : '';?>" />
				</div>

				<div class="se-wrapper__links">
					<div class="se-header">Links</div>
					<label class="se-label">
						label <input class="se-input" type="text" placeholder="enter link label" id="se-link-label" name="se-link-label" value="<?php echo $link ? $link['label'] : ''; ?>" />
					</label>
					<label class="se-label">
						URL <input class="se-input" type="url" placeholder="enter event url" id="se-link-url" name="se-link-url"  value="<?php echo $link ? $link['url'] : ''; ?>" />
					</label>
				</div>
			</div>

		<?php
	}

	/**
	 * Handle saving meta data for an event.
	 * @param  int $post_id current post id.
	 * @param  WP_Post $post    current post
	 */
	public function save_event_metaboxes( $post_id, $post ) {

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE || 
			'revision' === $post->post_type || $this->plugin_name !== $post->post_type ||
			'auto-draft' === $post->post_status || ! current_user_can( 'edit_post', $post_id ) ) {

			return $post_id;
		}

		// verify the nonce.
		if ( ! isset( $_POST['event_metabox_nonce'] ) || ! wp_verify_nonce( $_POST['event_metabox_nonce'], 'create_event_details' ) ) {
			return $post_id;
		}

		// url referer for redirecting back if there was an error.
		$referer = add_query_arg( array( 'post' => $post_id, 'action' => 'edit' ), admin_url( 'post.php' ) );

		// required meta fields.
		if ( empty( $_POST['se-start-date'] ) ) {
			wp_redirect( add_query_arg( 'simply_events_error', 1, $referer ) );
			exit;
		}

		// if no end date specified then we use 11:59 PM as the end time of the same day.
		if ( empty( $_POST['se-end-date'] ) ) {
			$_POST['se-end-date'] = $_POST['se-start-date'];
			$_POST['se-end-hour'] = '';
		}

		$coords = false;

		// are we using google map?
		if ( $this->using_google_map() && !empty( $_POST['se-location-lat'] ) && !empty( $_POST['se-location-lng' ] ) ) {

			// checking longitude/latitude coordinates
			$coordinates = "{$_POST['se-location-lat']},{$_POST['se-location-lng']}";
			if ( ! preg_match( '/^(\-?\d+(\.\d+)?),\s*(\-?\d+(\.\d+)?)$/', $coordinates ) ) {
				wp_redirect( add_query_arg( 'simply_events_error', 5, $referer ) );
				exit;
			}

			$coords = array( 'lat' => $_POST['se-location-lat'], 'lng' => $_POST['se-location-lng'] );
		}

		// processing start date time. Default to 12 am if no starting hour specified.
		$_POST['se-start-hour'] = empty( $_POST['se-start-hour'] ) ? '12' : $_POST['se-start-hour'];

		// default start minutes if empty.
		if ( empty( $_POST['se-start-min'] ) ) {
			$_POST['se-start-min'] = '00';
		} elseif ( 10 > (int) $_POST['se-start-min'] ) {
			// minutes need to be in MM format
			$_POST['se-start-min'] = '0' . (int) $_POST['se-start-min'];
		}
		$_POST['se-select-start-meridian'] = empty( $_POST['se-select-start-meridian'] ) ? 'AM' : $_POST['se-select-start-meridian'];

		$starttime = new DateTime( "{$_POST['se-start-hour']}:{$_POST['se-start-min']} {$_POST['se-select-start-meridian']} {$_POST['se-start-date']}" );
		if ( ! $starttime ) {
			wp_redirect( add_query_arg( 'simply_events_error', 2, $referer ) );
			exit;
		}

		// processing end date time. Default to 11:59 PM if no end hour specified.
		if ( empty( $_POST['se-end-hour'] ) ) {
			$_POST['se-end-hour']            = '11';
			$_POST['se-end-min']             = '59';
			$_POST['se-select-end-meridian'] = 'PM';
		}

		if ( empty( $_POST['se-end-min'] ) ) {
			$_POST['se-end-min'] = '00';
		} elseif ( 10 > (int) $_POST['se-end-min'] ) {
			// minutes need to be in MM format
			$_POST['se-end-min'] = '0' . (int) $_POST['se-end-min'];
		}
		$_POST['se-select-end-meridian'] = empty( $_POST['se-select-end-meridian'] ) ? 'PM' : $_POST['se-select-end-meridian'];

		$endtime = new DateTime( "{$_POST['se-end-hour']}:{$_POST['se-end-min']} {$_POST['se-select-end-meridian']} {$_POST['se-end-date']}" );
		if ( ! $endtime ) {
			wp_redirect( add_query_arg( 'simply_events_error', 2, $referer ) );
			exit;
		}

		// not a valid end date if it has passed or the same as the starting date =/
		if ( $starttime >= $endtime ) {
			wp_redirect( add_query_arg( 'simply_events_error', 4, $referer ) );
			exit;
		}

		// check if is a recurring event.
		if ( !empty( $_POST['se-repeat'] ) && 'true' === $_POST['se-repeat'] ) {

			$repeat_data = array();
			$i = 'D';
    		$whitelist = array(
				'freq'     		  => array( 'daily', 'weekly', 'monthly', 'yearly' ),
				'month_repeat_by' => array( 'dom', 'dow' ),
				'end'             => array( 'never', 'date', 'occurrence' )
    		);

			$freq = empty( $_POST['se-repeat-frequency'] ) || !in_array( $_POST['se-repeat-frequency'], $whitelist['freq'] )
					   			 ? 'daily'
					   		     : $_POST['se-repeat-frequency'];

			$interval = empty( $_POST['se-repeat-interval'] ) ? 1 : $_POST['se-repeat-interval'];
			$interval = (int) $interval;

			// check interval range between 1-30
			if( 1 > $interval || 30 < $interval ) $interval = 1;

			if ( 'weekly' === $freq ) {
				$repeat_data['repeat_weekly_day'] = array();
				if ( !empty( $_POST['se-repeat-weekly-day'] ) ) {
					foreach ( $_POST['se-repeat-weekly-day'] as $day ) {
						if ( 0 <= (int) $day && 6 >= (int) $day ) {
							$repeat_data['repeat_weekly_day'][] = $day;
						}
					}
				}

				// default to current day of starting date.
				if ( empty( $repeat_data['repeat_weekly_day'] ) ) {
					$repeat_data['repeat_weekly_day'][] = ((int) $starttime->format( 'w' ) + 6) % 7;
				}

				$i = 'W';

			} elseif ( 'monthly' === $freq ) {
				// default to dom
				if ( empty( $_POST['se-repeat-monthly-repeatby'] )
					|| !in_array( $_POST['se-repeat-monthly-repeatby'], $whitelist['month_repeat_by'] ) ) {

					$repeat_data['repeat_monthly_by'] = 'dom';
				} else {
					$repeat_data['repeat_monthly_by'] = $_POST['se-repeat-monthly-repeatby'];
				}

			} else if ( 'yearly' === $freq ) {
				$i = 'Y';
			}

			$repeat_data['end'] = empty( $_POST['se-repeat-end'] ) || !in_array( $_POST['se-repeat-end'], $whitelist['end'] )
								? 'never'
								: $_POST['se-repeat-end'];

			if ( 'occurrence' === $repeat_data['end'] ) {
				$repeat_data['end_occurrence'] = empty( $_POST['se-repeat-end-occurrence'] ) ? 1 : (int) $_POST['se-repeat-end-occurrence'];

				// if occurrence out of range (1-100) then set to 1
				if ( 0 > $repeat_data['end_occurrence'] || 100 < $repeat_data['end_occurrence'] ) {
					$repeat_data['end_occurrence'] = 1;
				}
			} else if ( 'date' === $repeat_data['end'] ) {
				if ( empty( $_POST['se-repeat-end-date'] ) ) {
					$repeat_data['end_date'] = $endtime;
				} else {
					$repeat_data['end_date'] = new DateTime( $_POST['se-repeat-end-date'] );

					$ts = $repeat_data['end_date']->getTimestamp();

					// if the specified end date is earlier or equal to the event start date
					// then re adjust the specified date to the events end date.
					if ( $ts <= $starttime->getTimestamp() ) $repeat_data['end_date'] = $endtime;
				}

				$repeat_data['end_date'] = $repeat_data['end_date']->getTimestamp();
			}

			// keep a timestamp of the difference with the interval.
			$new_starttime = clone $starttime;
			$new_starttime->add( new DateInterval( "P{$interval}{$i}" ) );
			$diff = $new_starttime->getTimestamp() - $starttime->getTimestamp();

			// @ note if event start time day < any of the selected days we should adjust the new event start time to min of those days.

			update_post_meta( $post_id, 'simply_events_repeat_freq', $freq );
			update_post_meta( $post_id, 'simply_events_repeat_interval', $interval );
			update_post_meta( $post_id, 'simply_events_repeat_data', $repeat_data );
			update_post_meta( $post_id, 'simply_events_repeat_diff', $diff );

			unset( $repeat_data );
		} else {
			// delete repeat
			delete_post_meta( $post_id, 'simply_events_repeat_freq' );
			delete_post_meta( $post_id, 'simply_events_repeat_interval' );
			delete_post_meta( $post_id, 'simply_events_repeat_data' );
			delete_post_meta( $post_id, 'simply_events_repeat_diff' );
		}

		// we made it. phew.

		// convert timestamp to a common date format on the database.
		update_post_meta( $post_id, 'simply_events_start_datetime', $starttime->getTimestamp() );
		update_post_meta( $post_id, 'simply_events_end_datetime', $endtime->getTimestamp() );

		if ( !empty( $_POST['se-location'] ) ) {
			update_post_meta( $post_id, 'simply_events_location', $_POST['se-location'] );

			// serialized array of coordniates.
			if ( $coords ) update_post_meta( $post_id, 'simply_events_coordinates', $coords );

		} else {
			delete_post_meta( $post_id, 'simply_events_location' );
			delete_post_meta( $post_id, 'simply_events_coordinates' );
		}

		if ( !empty( $_POST['se-link-url'] ) && filter_var( $_POST['se-link-url'], FILTER_VALIDATE_URL ) ) {
			$link = array(
				'url' => $_POST['se-link-url'],
				'label' => !empty( $_POST['se-link-label'] ) ? $_POST['se-link-label'] : 'Go to event'
			);

			update_post_meta( $post_id, 'simply_events_link', $link );

		} else {
			delete_post_meta( $post_id, 'simply_events_link' );
		}

	}

	/**
	 * Checks if options are set for using google map.
	 */
	public function using_google_map() {

		$options = get_option( $this->options_name );

		if ( !empty( $options['use_google_map'] ) && !empty( $options['google_map_key'] ) ) {
			return $options['google_map_key'];
		}

		return false;
	}

	/**
	 * Output the error messages used.
	 * Used for admin_notices hook.
	 * 
	 * $_GET['simply_events_error'] should be the error code from the $this->errors
	 * 
	 * @todo use pagenow to get only show errors in the edit post page
	 */
	public function error_messages() {
		if ( ! isset( $_GET['simply_events_error'] ) || ! isset( $this->errors[ $_GET['simply_events_error'] ] ) ) {
			return;
		}

		$message = $this->errors[ $_GET['simply_events_error'] ];
		echo '<div class="notice notice-error is-dismissible"><p>' . __( $message, 'simply-events' ) . '</p></div>';
	}


	/**
	 * Validation for option fields
	 * @param  array $inputs array of stored option inputs.
	 * @return array new option inputs.
	 */
	public function validate_options( $inputs ) {
		return $inputs;
	}

	public function settings_page() {
		/**
		 * Add a settings page under our custom post type menu.
		 *
		 * @link https://developer.wordpress.org/reference/functions/add_submenu_page
		 */
        add_submenu_page(
        	"edit.php?post_type={$this->plugin_name}",
        	'Settings',
        	'Settings',
        	'manage_options',
        	'simply-events-settings',
        	array( $this, 'render_settings_page' )
        );
	}

	/**
	 * Callback for rendering the settings page for our custom post type.
	 */
	public function render_settings_page() {
		?><div class="wrap">
			<h2>Simply Events Settings</h2>
			<form action="options.php" method="post">
				<?php settings_fields( $this->options_name ); ?>
				<?php do_settings_sections( $this->plugin_name ); ?>
				<input name="Submit" type="submit" value="<?php esc_attr_e( 'Save Changes' ); ?>" />
			</form>
		</div><?php
	}
}