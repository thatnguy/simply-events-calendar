(function( $ ) {
	'use strict';

	// event recurrence section.

	var $repeatBtn = $( '#se-repeat-btn' ),
		$repeatWrapper = $( '.date-repeat-wrapper__wrap' ),
		$repeatFreq = $( '#se-repeat-frequency' ),
		$repeatInt = $( '#se-repeat-interval' ),
		repeatFreqMap = {
			daily: 'day', weekly: 'week', monthly: 'month', yearly: 'year'
		},
		repeatFreqPrev = $repeatFreq.val();

	$repeatBtn.on( 'click', function( e ) {
		e.stopPropagation();

		var checked = this.checked;

		$repeatWrapper.toggleClass( 'hidden', !checked );

		this.value = checked;

	});

	$repeatFreq.on( 'change', function( e ) {
		var value = this.value;

		if ( !repeatFreqMap[ value ] ) return;

		var t = repeatFreqMap[ value ];

		if ( repeatFreqPrev ) {
			$repeatWrapper.find( '[data-toggle-ref="' +repeatFreqPrev+ '"]' ).addClass( 'hidden' );
		}

		$repeatInt.next( '.se-repeat-interval-text' ).text( t + 's' );

		$repeatInt.children( 'option' ).each( function( i, el ) {
			el.setAttribute( 'title', 'Repeats every ' +( i+1 )+ ' ' +t +( 0 < i ? 's' : '' ) );
		});

		repeatFreqPrev = value;

		// toggle any hidden sections for selected value.
		$repeatWrapper.find( '[data-toggle-ref="' +value+ '"]' ).removeClass( 'hidden' );
	});

	$repeatWrapper.on( 'change', '#se-repeat-end-date, #se-repeat-end-occurrences, #se-repeat-end-never', function( e ) {
		// disable sibling input
	});

	// datepicker.
	var $startDate = $( '#se-start-date' ),
		$endDate   = $( '#se-end-date' ),
		$repeatEndDate = $( '#se-repeat-end-date-selection' );

	if ( 1 === $startDate.length && 1 === $endDate.length ) {

		$startDate.datepicker( {
			onSelect: function( str ) {

				// update minimum end date.
				// this ensures that end date is >= start date
				var d = $( this ).datepicker( 'getDate' );
				$endDate.datepicker( 'option', 'minDate', d );
				$repeatEndDate.datepicker( 'option', 'minDate', d );
			}
		} );

		$endDate.datepicker();
	}

	$repeatEndDate.datepicker();

	// google places api
	var autocomplete = null;

	var loc = document.getElementById( 'se-location' ),
		lat = document.getElementById( 'se-location-lat' ),
		lng = document.getElementById( 'se-location-lng' );

	function initialize() {
		if ( null === loc || null === lat || null === lng ) {
			return false;
		}
		
		// no address provided then we check if latitude and longitude has been given.
		if ( '' === loc.value && '' !== lat.value && '' !== lng.value ) {
			var latlng = new google.maps.LatLng( lat.value, lng.value );
			var geocoder = new google.maps.Geocoder();

			geocoder.geocode( { 'latLng': latlng }, function( results, status ) {
				if ( google.maps.GeocoderStatus.OK === status && results[0] ) {
					// formatted_address is not same format as how the autocomplete formats the address
					// @todo change to the autocomplete's format @see adr_address attribute in place object.
					loc.value = results[0].formatted_address;
				}
			} );
		}

		autocomplete = new google.maps.places.Autocomplete( loc, { types: ['geocode'], componentRestrictions: { country: 'au' } } );

		autocomplete.addListener( 'place_changed', saveCoordinates );
	}

	function saveCoordinates() {
		var place = autocomplete.getPlace();

		// save the lat and lng
		lat.value = place.geometry.location.lat();
		lng.value = place.geometry.location.lng();
	}

	// make function public so google map can call.
	window.googleMapInit = initialize;

})( jQuery );
