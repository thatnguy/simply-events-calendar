<?php
/**
 * Public hooks are defined here.
 *
 * @package  Simply Events
 */
class Simply_Events_Public {
	
	private $plugin_name;

	private $version;

	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 */
	public function enqueue_styles() {

		$script_uri = plugin_dir_url( __FILE__ ) . 'css';

		wp_enqueue_style( $this->plugin_name, "{$script_uri}/simply-events.css", array(), $this->version, 'all' );

		wp_enqueue_style( 'load-spinner', "{$script_uri}/load-spinner.css" );
		
		wp_enqueue_style( 'dashicons' );
 
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 */
	public function enqueue_scripts() {

		$script_uri = plugin_dir_url( __FILE__ ) . 'js';

		// enqueue our simple cache.
		wp_enqueue_script( 'simply-events-cache', "{$script_uri}/simply-events-cache.js", array(), $this->version, true );

		wp_enqueue_script( 'load-spinner', "{$script_uri}/load-spinner.js", array(), $this->version, true );

		// enqueue public facing script.
		wp_enqueue_script( $this->plugin_name, "{$script_uri}/simply-events.js", array(), $this->version, true );
	}

	/**
	 * Add common response data.
	 * @see   https://developer.wordpress.org/rest-api/extending-the-rest-api/modifying-responses/
	 * @param  WP_REST_Response $response current Response object
	 * @param  WP_Post $post     current post object
	 * @return WP_REST_Response           filtered response object.
	 */
	public function rest_prepare_response( $response, $post, $request ) {

		if ( !empty( $post->post_author ) && 0 < $post->post_author	) {
			$user = get_userdata( $post->post_author );

			$response->data['organizer'] = array(
				'id'    => $user->ID,
				'name'  => $user->data->display_name,
				'email' => $user->data->user_email,
			);
		}

		// just remove link if dont have in DB.
		if ( isset( $response->data['link'] ) && !$response->data['link'] ) {
			unset( $response->data['link'] );
		}

		if ( isset( $response->data['coordinates'] ) && !$response->data['coordinates'] ) {
			unset( $response->data['coordinates'] );
		}

		// format the timestamp dates to 'Y-m-d H:i:s' standard format.
		$dt = new DateTime;

		$dt->setTimestamp( $response->data['start_datetime'] );
		$response->data['start_datetime'] = $dt->format( 'Y-m-d H:i:s' );

		$dt->setTimestamp( $response->data['end_datetime'] );
		$response->data['end_datetime'] = $dt->format( 'Y-m-d H:i:s' );

		return $response;
	}

	/**
	 * Add meta fields into WP REST Response
	 */
	public function rest_add_meta_fields() {
		// add event date field
	    register_rest_field(
	    	'simply-events',				// post type
	    	'start_datetime',				// field name, this will be passed to the callback below.
	    	array(
				'get_callback'    => array( $this, 'get_event_meta' ),
				// not updatable.
				'update_callback' => null,
				'schema'          => null
	    	)
	    );
	    register_rest_field(
	    	'simply-events',
	    	'end_datetime',
	    	array(
				'get_callback'    => array( $this, 'get_event_meta' ),
				'update_callback' => null,
				'schema'          => null
	    	)
	    );

	    // add event location field
	    register_rest_field(
	    	'simply-events',
	    	'location', 
	    	array(
				'get_callback'    => array( $this, 'get_event_meta' ),
				'update_callback' => null,
				'schema'          => null
	    	)
	    );

	    register_rest_field(
	    	'simply-events',
	    	'link',
	    	array(
	    		'get_callback'    => array( $this, 'get_event_meta' ),
				'update_callback' => null,
				'schema'          => null
	    	)
	    );

	    register_rest_field(
	    	'simply-events',
	    	'coordinates',
	    	array(
	    		'get_callback'    => array( $this, 'get_event_meta' ),
				'update_callback' => null,
				'schema'          => null,
	    	)
	    );

	    register_rest_field(
	    	'simply-events',
	    	'repeat_data',
	    	array(
	    		'get_callback' => array( $this, 'get_event_meta' ),
	    	)
	    );
	}

	public function get_event_meta( $object, $field_name, $request ) {
		$key = "simply_events_{$field_name}";
		return get_post_meta( $object['id'], $key, true );
	}

	public function allow_meta_query( $valid_vars ) {
		$valid_vars[] = 'meta_query';

		return $valid_vars;
	}

	public function add_rest_query_vars( $valid_vars ) {
		// allow `OR` relation in the POST_ID IN () query instead of the default `AND`
		$valid_vars[] = 'or_post__in';

		return $valid_vars;
	}

	public function events_where_clause( $where, $query ) {
		global $wpdb;

		$query_vars = $query->query_vars;

		if ( $this->plugin_name === $query_vars['post_type'] && isset( $query_vars['or_post__in'] ) && is_array( $query_vars['or_post__in'] ) ) {

			$where .= " OR {$wpdb->posts}.ID IN (";
			foreach ( $query_vars['or_post__in'] as $id ) $where .= "{$id},";

			$where = rtrim( $where, ',' ) . ')';
		}

		return $where;
	}

	public function register_routes( $server ) {
		require_once dirname( __FILE__ ) . '/simply-events-controller.class.php';

		$controller = new Simply_Events_Controller( $this->plugin_name, "simply-events-api" );

		$controller->register_routes();
	}
}