<?php
	
	class Simply_Events_Controller extends WP_REST_Posts_Controller {
		public function __construct( $post_type, $namespace = '' ) {
			parent::__construct( $post_type );

			$this->namespace = $namespace;
		}

		public function register_routes() {
			parent::register_routes();
		}

		// modifying parent::get_items()
		// @see WP_REST_Posts_Controller->get_items()
		public function get_items( $request ) {

			//Make sure a search string is set in case the orderby is set to 'relevace'
			if ( ! empty( $request['orderby'] ) && 'relevance' === $request['orderby'] && empty( $request['search'] ) && empty( $request['filter']['s'] ) ) {
				return new WP_Error( 'rest_no_search_term_defined', __( 'You need to define a search term to order by relevance.' ), array( 'status' => 400 ) );
			}

			$args                         = array();
			$args['author__in']           = $request['author'];
			$args['author__not_in']       = $request['author_exclude'];
			$args['menu_order']           = $request['menu_order'];
			$args['offset']               = $request['offset'];
			$args['order']                = $request['order'];
			$args['orderby']              = $request['orderby'];
			$args['paged']                = $request['page'];
			$args['post__in']             = $request['include'];
			$args['post__not_in']         = $request['exclude'];
			$args['name']                 = $request['slug'];
			$args['post_parent__in']      = $request['parent'];
			$args['post_parent__not_in']  = $request['parent_exclude'];
			$args['post_status']          = $request['status'];
			$args['s']                    = $request['search'];

			$args['date_query'] = array();
			// Set before into date query. Date query must be specified as an array of an array.
			if ( isset( $request['before'] ) ) {
				$args['date_query'][0]['before'] = $request['before'];
			}

			// Set after into date query. Date query must be specified as an array of an array.
			if ( isset( $request['after'] ) ) {
				$args['date_query'][0]['after'] = $request['after'];
			}

			if ( is_array( $request['filter'] ) ) {
				$args = array_merge( $args, $request['filter'] );
				unset( $args['filter'] );
			}

			// Ensure our per_page parameter overrides filter.
			$args['posts_per_page'] = $request['per_page'];

			if ( isset( $request['sticky'] ) ) {
				$sticky_posts = get_option( 'sticky_posts', array() );
				if ( $sticky_posts && $request['sticky'] ) {
					// As post__in will be used to only get sticky posts,
					// we have to support the case where post__in was already
					// specified.
					$args['post__in'] = $args['post__in'] ? array_intersect( $sticky_posts, $args['post__in'] ) : $sticky_posts;

					// If we intersected, but there are no post ids in common,
					// WP_Query won't return "no posts" for `post__in = array()`
					// so we have to fake it a bit.
					if ( ! $args['post__in'] ) {
						$args['post__in'] = array( -1 );
					}
				} elseif ( $sticky_posts ) {
					// As post___not_in will be used to only get posts that
					// are not sticky, we have to support the case where post__not_in
					// was already specified.
					$args['post__not_in'] = array_merge( $args['post__not_in'], $sticky_posts );
				}
			}

			// Force the post_type argument, since it's not a user input variable.
			$args['post_type'] = $this->post_type;
			
			if ( isset( $request['start_date'] ) ) {
				$start_date = new DateTime( $request['start_date'] );
				$end_date   = new DateTime();
				$end_date->setDate( $start_date->format('Y'), $start_date->format('m'), $start_date->format('t') );
				$end_date->setTime( 23, 59, 59 );

				if ( isset( $request['end_date'] ) ) {
					$new_end_date = new DateTime( $request['end_date'] );
					if ( $new_end_date->getTimestamp() > $start_date->getTimestamp() ) $end_date = $new_end_date;
				}

				$request['simply_events_timestamps'] = array( 'start' => $start_date->getTimestamp(), 'end' => $end_date->getTimestamp() );
			}

			$args = apply_filters( "rest_{$this->post_type}_query", $args, $request );

			$args = $this->add_event_meta_queries( $args, $request );

			$query_args = $this->prepare_items_query( $args, $request );

			$taxonomies = wp_list_filter( get_object_taxonomies( $this->post_type, 'objects' ), array( 'show_in_rest' => true ) );
			foreach ( $taxonomies as $taxonomy ) {
				$base = ! empty( $taxonomy->rest_base ) ? $taxonomy->rest_base : $taxonomy->name;
				$tax_exclude = $base . '_exclude';

				if ( ! empty( $request[ $base ] ) ) {
					$query_args['tax_query'][] = array(
						'taxonomy'         => $taxonomy->name,
						'field'            => 'term_id',
						'terms'            => $request[ $base ],
						'include_children' => false,
					);
				}

				if ( ! empty( $request[ $tax_exclude ] ) ) {
					$query_args['tax_query'][] = array(
						'taxonomy'         => $taxonomy->name,
						'field'            => 'term_id',
						'terms'            => $request[ $tax_exclude ],
						'include_children' => false,
						'operator'         => 'NOT IN',
					);
				}
			}

			$posts_query = new WP_Query();
			$query_result = $posts_query->query( $query_args );

			// Allow access to all password protected posts if the context is edit.
			if ( 'edit' === $request['context'] ) {
				add_filter( 'post_password_required', '__return_false' );
			}

			$posts = array();
			foreach ( $query_result as $post ) {
				if ( ! $this->check_read_permission( $post ) ) {
					continue;
				}

				$data = $this->prepare_item_for_response( $post, $request );

				// if start date and end date set
				if ( isset( $request['simply_events_timestamps'] ) ) {
					$data = $this->prepare_event_recurring_dates(
						$data,
						$request['simply_events_timestamps']['start'],
						$request['simply_events_timestamps']['end']
					);

					if ( !$data ) continue;
				}

				// event featured image.
				if ( !empty( $data->data['featured_media'] ) ) {

					$image = get_post( $data->data['featured_media'] );
					if ( !empty( $image ) && 'attachment' === $image->post_type ) {
						$data->data['featured_media'] = array( 
							'id'    => $data->data['featured_media'],
							'title' => $image->post_title,
							'src'   => $image->guid,
							'alt'   => get_post_meta( $image->ID, '_wp_attachment_image_alt', true ),
						);
					}
				}

				$posts[] = $this->prepare_response_for_collection( $data );
			}

			// Reset filter.
			if ( 'edit' === $request['context'] ) {
				remove_filter( 'post_password_required', '__return_false' );
			}

			$page = (int) $query_args['paged'];
			$total_posts = $posts_query->found_posts;

			if ( $total_posts < 1 ) {
				// Out-of-bounds, run the query again without LIMIT for total count
				unset( $query_args['paged'] );
				$count_query = new WP_Query();
				$count_query->query( $query_args );
				$total_posts = $count_query->found_posts;
			}

			$max_pages = ceil( $total_posts / (int) $query_args['posts_per_page'] );

			$response = rest_ensure_response( $posts );
			$response->header( 'X-WP-Total', (int) $total_posts );
			$response->header( 'X-WP-TotalPages', (int) $max_pages );

			$request_params = $request->get_query_params();
			if ( ! empty( $request_params['filter'] ) ) {
				// Normalize the pagination params.
				unset( $request_params['filter']['posts_per_page'] );
				unset( $request_params['filter']['paged'] );
			}
			$base = add_query_arg( $request_params, rest_url( sprintf( '%s/%s', $this->namespace, $this->rest_base ) ) );

			if ( $page > 1 ) {
				$prev_page = $page - 1;
				if ( $prev_page > $max_pages ) {
					$prev_page = $max_pages;
				}
				$prev_link = add_query_arg( 'page', $prev_page, $base );
				$response->link_header( 'prev', $prev_link );
			}
			if ( $max_pages > $page ) {
				$next_page = $page + 1;
				$next_link = add_query_arg( 'page', $next_page, $base );
				$response->link_header( 'next', $next_link );
			}

			return $response;
		}

		public function add_event_meta_queries( $args, $request ) {
			$meta_queries = array();

			if ( isset( $request['simply_events_timestamps'] ) ) {
				// filter by events starting between start_date and end_date.
				$start = $request['simply_events_timestamps']['start'];
				$end   = $request['simply_events_timestamps']['end'];

				$meta_queries[] = array(
					'key' => 'simply_events_start_datetime',
					'value' => array( $start, $end ),
					'compare' => 'BETWEEN',
				);

				// get recurring events that also belong in this timeframe.
				$ids = $this->get_recurring_events( $start, $end );

				if ( !empty( $ids ) ) $args['or_post__in'] = $ids;
			}

			// filter events by location.
		    if ( isset( $request['location'] ) ) {
		    	$meta_queries[] = array(
		    		'key' => 'simply_events_location',
		    		'value' => $request['location'],
		    		'compare' => 'LIKE',
		    	);
		    }

			if ( !empty( $meta_queries ) ) $args['meta_query'] = $meta_queries;

			// get all events.
			$args['posts_per_page'] = -1;

			return $args;
		}


		public function prepare_event_recurring_dates( $response, $start, $end ) {
			// is an repeating event.
			if ( !empty( $response->data['repeat_data'] ) ) {

				$freq = get_post_meta( $response->data['id'], 'simply_events_repeat_freq', true );
				$interval = get_post_meta( $response->data['id'], 'simply_events_repeat_interval', true );
				$r_data = $response->data['repeat_data'];

				$start_dt = new DateTime( $response->data['start_datetime'] );
				$ev_start_ts = $start_dt->getTimestamp();
				$o_start_ts  = $start_dt->getTimestamp();	// keep an original timestamp

				$end_dt = new DateTime( $response->data['end_datetime'] );
				$duration = $end_dt->getTimestamp() - $ev_start_ts;

				if ( $ev_start_ts > $start ) $start = $ev_start_ts;

				if ( 'date' === $r_data['end'] ) {
					$new_end_dt = ( new DateTime() )->setTimestamp( $r_data['end_date'] );

					if ( $new_end_dt->getTimestamp() < $end ) $end = $new_end_dt->getTimestamp();
				}

				$repeats = array();
				$dt = new DateTime;

				$intervals = array( 'yearly'  => 'Y', 'monthly' => 'M', 'weekly'  => 'W', 'daily'   => 'D' );

				$ev_day = (int) $start_dt->format( 'N' );

				while ( true ) {

					if ( $ev_start_ts >= $start ) {
						$dt->setTimestamp( $ev_start_ts );

						if ( 'weekly' === $freq ) {
							$d = (int) $dt->format( 'N' );
							$d_ts = $dt->getTimestamp();

							foreach ( $r_data['repeat_weekly_day'] as $i => $day ) {
								$dt->setTimestamp( $d_ts + (( ++$day - $d ) * 86400) );
								$ts = $dt->getTimestamp();

								if ( 'date' === $r_data['end'] && (int) $r_data['end_date'] < $ts ) break;

								if ( $ts <= $end ) {
									$repeat = array();
									$repeat['start'] = $dt->format( 'Y-m-d H:i:s' );
									$dt->setTimestamp( $ts + $duration );
									$repeat['end'] = $dt->format( 'Y-m-d H:i:s' );
									$repeats[] = $repeat;
								}

								if ( 'occurrence' === $r_data['end'] && 1 > ($r_data['end_occurrence']-($i+1)) ) break;
							}
						}

						if ( $ev_start_ts > $end ) break;

						if ( 'weekly' !== $freq ) {
							if ( 'monthly' === $freq && 'dow' === $r_data['repeat_monthly_by'] ) {
								$ev_start_ts += ( $ev_day - (int) $start_dt->format( 'N' ) ) * 86400;
								$dt->setTimestamp( $ev_start_ts );
							}

							// only add recurring dates the recurring date is not the original event starting date.
							if ( $dt->getTimestamp() !== $o_start_ts ) {
								$repeat = array();
								$repeat['start'] = $dt->format( 'Y-m-d H:i:s' );
								$dt->setTimestamp( $dt->getTimestamp() + $duration );
								$repeat['end'] = $dt->format( 'Y-m-d H:i:s');
								$repeats[] = $repeat;
							}
						}

					}

					// move the start point to next interval
					$start_dt->add( new DateInterval( "P{$interval}{$intervals[$freq]}" ) );
					$ev_start_ts = $start_dt->getTimestamp();

					if ( 'occurrence' === $r_data['end'] ) {
						$r_data['end_occurrence'] -= 'weekly' === $freq ? count( $r_data['repeat_weekly_day'] ) : 1;

						if ( 1 > $r_data['end_occurrence'] ) break;
					}
				}

				if ( !empty( $repeats ) ) $response->data['recurring_dates'] = $repeats;

				unset( $intervals );
				// unset( $response->data['repeat_data'] );

				if ( ( $start > $o_start_ts || $end < $end_dt->getTimestamp() ) && empty( $repeats ) ) {
					unset( $repeats );
					return false;
				}

				unset( $repeats );

			}	// if have repeat 

			return $response;
		}

		public function get_recurring_events( $start, $end ) {
			global $wpdb;

			if ( (int) $start > (int) $end ) {
				throw new WP_Error( 
					'get_recurring_events_timestamp_range',
					'inputted start timestamp can not be lower than the end timestamp'
				);
			}

			$query = "SELECT p.ID
					  FROM {$wpdb->posts} AS p
					  JOIN (
					  	SELECT m.meta_value, m.post_id
					  	FROM {$wpdb->postmeta} AS m
					  	WHERE m.meta_key = 'simply_events_start_datetime'
					  ) AS starttime ON starttime.post_id = p.ID
					  JOIN (
					  	SELECT m.meta_value, m.post_id
					  	FROM {$wpdb->postmeta} AS m
					  	WHERE m.meta_key = 'simply_events_repeat_diff'
					  ) AS diff ON diff.post_id = p.ID
					  JOIN (
						SELECT m.meta_value, m.post_id
						FROM {$wpdb->postmeta} AS m
						WHERE m.meta_key = 'simply_events_repeat_freq'
					  ) AS freq ON freq.post_id = p.ID
					  JOIN (
						SELECT m.meta_value, m.post_id
						FROM {$wpdb->postmeta} AS m
						WHERE m.meta_key = 'simply_events_repeat_interval'
					  ) AS i ON i.post_id = p.ID
					  WHERE (freq.meta_value = 'monthly' AND se_check_in_month(starttime.meta_value, %d, %d, i.meta_value) = 1) OR
					  	(freq.meta_value != 'monthly' AND starttime.meta_value <= %d AND (%d - ((%d - starttime.meta_value) %% diff.meta_value)) >= %d) AND p.post_type = %s";

			$results = $wpdb->get_results( $wpdb->prepare( $query, array(
				$start, $end, $end, $end, $end, $start, $this->post_type
			) ) );

			$ids = array();
			foreach ( $results as $result ) $ids[] = $result->ID;

			return $ids;
		}

	}