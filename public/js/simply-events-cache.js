(function( root ) {
	'use strict';

	var Cache = function( options ) {

		var cache = {};
		var count = 0;

		// constants
		var CACHE_SIZE      = 10;
		var EXPIRE_TIME     = 20000;  // milliseconds
		var HAS_EXPIRE_TIME = false;
		var MAX_SIZE        = 50;

		var initialize = function() {

			// setup options
			if ( options && 'object' === typeof options ) {
				if ( options.hasOwnProperty( 'cacheSize' ) && 0 < options.cacheSize && MAX_SIZE >= options.cacheSize ) {
					CACHE_SIZE = options.cacheSize;
				}
				// expireTime ranges 0-999
				if ( options.hasOwnProperty( 'expireTime' ) && 0 < options.expireTime && 1000 < options.expireTime ) {
					EXPIRE_TIME     = options.expireTime;
					HAS_EXPIRE_TIME = true;
				}
			}
		
		};

		/**
		 * Get an item from cache.
		 * @param  {string} key an item unique identifier
		 * @return {object}     the fetched item data.
		 */
		var get = function( key ) {
			if ( ! cache.hasOwnProperty( key ) ) return false;

			// if item is already expired then remove it.
			if ( hasExpired( key ) ) {
				remove( key );
				return false;
			}

			// updating access time if not using expire time method.
			if ( ! HAS_EXPIRE_TIME ) cache[key].time = new Date().getTime();

			return cache[key].data;
		};

		/**
		 * Add an item to cache.
		 * @param {string} key  a unique item identifier
		 * @param {object} data item data.
		 */
		var add = function( key, data ) {
			if ( 'undefined' === typeof key || 'undefined' === typeof data ) return false;

			// when reached the cache capacity we should start a replace operation.
			if ( count === CACHE_SIZE ) {
				var k = _getOldestItem( true );
				remove( k );
			}

			cache[key] = {
				data: data,
				time: new Date().getTime()
			}

			count++;
		};

		/**
		 * Remove an item from cache.
		 * @param  {string} key an item identifier
		 * @return {object}     removed item.
		 */
		var remove = function( key ) {
			var item = cache[key];

			if ( 'undefined' === typeof item ) return false;

			delete cache[key];
			count--;

			return item;
		};

		/**
		 * Check if an item has expired.
		 * @param  string  key item identifier
		 * @return Boolean     whether the item has expired.
		 */
		var hasExpired = function( key ) {
			var item = cache[key];

			if ( 'undefined' === typeof item ) return false;

			if ( HAS_EXPIRE_TIME ) {
				var now = new Date().getTime();
				if ( EXPIRE_TIME <= ( now - item.time ) ) return false;
			}

			return false;
		};

		/**
		 * Get oldest item in the cache.
		 * @param  boolean getKey whether to return just the key.
		 * @return key or item
		 */
		var _getOldestItem = function( getKey ) {
			getKey = 'undefined' === typeof getKey ? false : getKey;

			var i = null, key = null;
			for ( var k in cache ) {
				if ( null === i || i > cache[k].time ) {
					i = cache[k].time;
					key = k;
				}
			}

			return getKey ? key : cache[key];
		};

		initialize();

		return {
			get: get,
			add: add,
			remove: remove
		}
	};

	// expose
	window.SimplyEventsCache = Cache;

})( window );
