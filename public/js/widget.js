;(function( root, doc, $, Bb, undefined ) {

	'use strict';

	var months         = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
		monthsShort    = _.map( months, function( month ) { return month.charAt(0) + month.charAt(1) + month.charAt(2); } ),
		days           = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
		scrollbarWidth = getScrollbarWidth();

	var getDaysInMonth = function( month, year ) {
		var days = [31, isLeapYear( year ) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];
		return days[month];
	}

	function isLeapYear( year ) {
		return ( 0 === year % 4 && 0 !== year % 100 ) || 0 === year % 400;
	}

	function getDaysInMonth( month, year ) {
		return [31, isLeapYear( year ) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ][month];
	}


	function isDate( date ) {
		return ( /Date/ ).test( Object.prototype.toString.call( date ) ) && ! isNaN( date.getTime() );
	}

	function padNumber( num ) {
		num = num * 1;

		if ( !isNaN( num ) && 10 > num ) num = '0' + num;

		return num;
	}

	function getScrollbarWidth() {
		var div = document.createElement( 'div' ),
			scrollbarWidth = null;

		div.style.width = '100px';
		div.style.overflow = 'scroll';
		div.style.msOverflowStyle = "scrollbar"; 	// window JS App.

		// mac OS will not work as scrollbar can be floated ontop of content.
		// therefore will not be able to find the difference in width.

		doc.body.appendChild( div );
		
		scrollbarWidth = div.offsetWidth - div.clientWidth;

		doc.body.removeChild( div );

		return scrollbarWidth;
	}

	function throttleEvent( obj, type, callback, delay ) {
		obj = obj || window;
		delay = delay || 250;

		var running = false,
			func    = function( e ) {
				if ( running )
					return;

				running = true;

				setTimeout( function() {
					requestAnimationFrame( function() {
						callback.apply( obj, [e] );

						running = false;
					} );
				}, delay );
			};

		obj.addEventListener( type, func );
	};

	// @todo check out jQuery extend .. for objects cloning of Date, RegEx..
	var clone = function( obj ) {

		if ( ! _.isObject( obj ) || _.isFunction( obj ) ) {
			return obj;
		}

		if ( _.isArray( obj ) ) {
			return obj.slice();
		}

		var cloneObj = {};
		for ( var key in obj ) {
			cloneObj[ key ] = clone( obj[ key ] );
		}

		return cloneObj
	};


	// localized data.
	var _data = {
		url: simply_events_data.url,
		nonce: simply_events_data.nonce,
		id: simply_events_data.widget_id,
		title: simply_events_data.title,
		container_class: simply_events_data.container_class
	};

	var Event = Bb.Model.extend({
		defaults: {
			// hidden away from selection. Useful for filtering a model from collection without removing the model.
			hidden: false,

			// this attribute should reflect whether the model is currently viewed
			selected: false
		},

		initialize: function() {
			var sDate = new Date( this.get( 'start_datetime' ) ),
				eDate = new Date( this.get( 'end_datetime' ) );

			this.set( 'start_datetime', this.updateDateTime( sDate ) );
			this.set( 'end_datetime', this.updateDateTime( eDate ) );
		},

		updateDateTime: function( date ) {
			var h = date.getHours(),
				dateTime = {
					year: date.getFullYear(),
					month: date.getMonth() + 1,
					day: date.getDate(),
					hour: ( (h + 11) % 12 ) + 1,
					minute: date.getMinutes(),
					meridiem: 12 <= h ? 'pm' : 'am'
				};

			return dateTime;
		}
	});

	var Events = Bb.Collection.extend({
		model: Event,

		url: _data.url,

		fetch: function( options ) {
			options = options || {};

			options.beforeSend = function( xhr ) {
				xhr.setRequestHeader( 'X-WP-Nonce', _data.nonce );
			};

			return Bb.Collection.prototype.fetch.call( this, options );
		}

	});

	var EventView = Bb.View.extend({
		tagName: 'div',

		// hide the item initially.
		className: 'simply-events-item hide',

		/**
		 * UnderscoreJS templating
		 *
		 * Interpolating
		 * 		Use <%= ... %> for non HTML escaping
		 * 		use <%- ... %> for HTML escaping
		 * 		use <% ... %> for JS code 
		 */
		template: _.template(
			'<div class="event-title" title="<%- title %>"><%- title %></div>\
			 <div class="simply-events-item__content">\
			 	<% if ( featured_media.src ) { %>\
		 		<div class="event-media">\
		 			<img src="<%- featured_media.src %>" alt="<%- featured_media.alt %>" title="<%- featured_media.title %>" />\
		 		</div><% } %>\
				<div class="event-location event-detail-block" title="event location">\
					<span class="se-icon dashicons dashicons-location"></span>\
					<span class="se-text"><%- location %></span>\
				</div>\
				<div class="event-date event-detail-block" title="event period">\
					<span class="se-icon dashicons dashicons-clock"></span>\
					<span class="se-text"><%- start_time %> <%- start_date %> to <br />\
						<%- end_time %> <%- end_date %>\
					    <% if ( is_repeat ) { %><br /><span class="event-recurring-label">Recurring Event</span><% } %>\
					</span>\
				</div>\
				<div class="event-organizer event-detail-block" title="event organizer">\
					<span class="se-icon dashicons dashicons-admin-users"></span>\
					<span class="se-text"><% if ( organizer ) %><%- organizer.name %></span>\
				</div>\
				<div class="event-desc event-detail-block"><%= content %></div>\
				<% if ( link ) { %><div class="event-link"><a href="<%= link.url %>"><%- link.label %></a></div><% } %>\
			</div>'
		),

		initialize: function() {
			// element attributes for date and time.
			var dateTime = this.model.get( 'start_datetime' );
			if ( dateTime ) {
				this.$el.attr({
					year     : dateTime.year,
					month    : dateTime.month,
					day      : dateTime.day,
					time     : dateTime.hour + '-' + dateTime.minute,
					meridiem : dateTime.meridiem
				});
			}

			this.listenTo( this.model, 'change:selected', this.toggle );
		},

		render: function() {
			var startDate = this.model.get( 'start_datetime' ),
				endDate   = this.model.get( 'end_datetime' ),
				data = {
					title      : this.model.get( 'title' ).rendered,
					content    : this.model.get( 'content' ).rendered,
					location   : this.model.get( 'location' ),
					start_date : padNumber( startDate.day )+ ' ' +months[startDate.month - 1]+ ' ' +startDate.year,
					end_date   : padNumber( endDate.day )+ ' ' +months[endDate.month - 1]+ ' ' +endDate.year,
					start_time : padNumber( startDate.hour )+ ':' +padNumber( startDate.minute )+ ' ' +startDate.meridiem,
					end_time   : padNumber( endDate.hour )+ ':' +padNumber( endDate.minute )+ ' ' +endDate.meridiem,
					link       : this.model.get( 'link' ),
					is_repeat  : this.model.get( 'is_repeat_instance' )
				};

			this.$el.html( this.template( _.extend( this.model.toJSON(), data ) ) );

			var $content = this.$el.find( '.simply-events-item__content' );

			$content.css( 'right', ( 0 < scrollbarWidth ? '-' +scrollbarWidth : 0 ) + 'px' );

			return this;
		},

		toggle: function() {
			var selected = this.model.get( 'selected' ),
				hidden   = this.model.get( 'hidden' );

			this.$el.toggleClass( 'hide', !selected || hidden );
		}
	});

	var EventsView = Bb.View.extend({
		tagName: 'div',

		className: 'simply-events-calendar-events events hide',

		events: {
			'click .close-events'                 : 'closeEvents',
			'click .event-item-selector.previous' : 'previousEvent',
			'click .event-item-selector.next'     : 'nextEvent'
		},

		initialize: function() {

			this.currEvents = [];
			this.currEventIndex = null;

			this.render();

			this.listenTo( this.collection, 'add', this.addEvent );
		},

		addEvent: function( event ) {
			var eventView = new EventView({ model: event });

			this.eventsList.append( eventView.render().el );

			return this;
		},

		render: function() {
			this.$el.empty();

			this.close  = $(
				'<button type="button" class="close-events se-button" title="close">\
					<span class="dashicons dashicons-no-alt"></span>\
				</button>'
			);
			this.prev   = $( 
				'<button type="button" class="event-item-selector previous se-button hide" title="previous">\
					<span class="dashicons dashicons-arrow-left-alt2"></span>\
				</button>'
			);
			this.next   = $( 
				'<button type="button" class="event-item-selector next se-button hide" title="next">\
					<span class="dashicons dashicons-arrow-right-alt2"></span>\
				</button>' 
			);
			this.eventsList = $( '<div class="event-items"></div>' );

			this.$el.append( this.close )
					.append( this.prev )
			 		.append( this.eventsList )
			 		.append( this.next );

			this.delegateEvents( this.events );

			return this;
		},

		reRender: function( events ) {
			this.render();

			if ( this.collection && 0 < this.collection.length ) {
				var that = this,
					toAdd = [],
					toRemove = [];
				this.collection.each( function( event ) {
					var recurring = event.get( 'recurring_dates'),
						isRepeat  = event.get( 'is_repeat_instance' );

					if ( recurring && !isRepeat ) {
						for ( var i = 0, l = recurring.length; i < l; ++i ) {
							var attrs = clone( event.attributes );

							attrs.id = ( "" + attrs.id + (i+1) ) * 1;
							attrs.start_datetime = recurring[i].start;
							attrs.end_datetime = recurring[i].end;
							attrs.is_repeat_instance = true;
							attrs.recurring_dates = [];

							toAdd.push( attrs );
							toRemove.push( event.get( 'id' ) );
						}
					} else {
						that.addEvent( event );
					}

				});

				if ( 0 < toRemove.length ) this.collection.remove( toRemove );

				if ( 0 < toAdd.length ) this.collection.add( toAdd );
			}

			return this;
		},

		// we assume same month and year for comparison and
		// we only check the day
		toggleEvents: function( day ) {
			// here we assume 
			day = day * 1;

			this.currEvents = [];
			this.currEventIndex = null;
			
			var that = this;
			this.collection.each( function( event, i ) {

				var start = event.get( 'start_datetime' ).day * 1,
					end   = event.get( 'end_datetime' ).day * 1,
					hidden = event.get( 'hidden' );


				// if start.day < d and d <= end
				if ( ( start <= day && end >= day ) && !hidden ) that.currEvents.push( i );

				// reset selection.
				event.set( 'selected', false );
			});

			if ( 0 < this.currEvents.length ) {

				// only show prev and next events selectors if we have more than one event
				this.prev.toggleClass( 'hide', 2 > this.currEvents.length );
				this.next.toggleClass( 'hide', 2 > this.currEvents.length );

				this.collection.at( this.currEvents[0] ).set( 'selected', true );
				this.currEventIndex = 0;
			}

			/**
			 * We need to show the events element first before we can adjust the position.
			 * This is because display: none is not rendered in the browser therefore the element's
			 * width and height is 0.
			 */
			this.$el.toggleClass( 'hide', 1 > this.currEvents.length );

		},

		// close the list of events currently selected.
		closeEvents: function( e ) {
			this.toggleEvents( undefined );

			return false;
		},

		// navigate previous event item in the currently selected event list.
		previousEvent: function( e ) {
			this.show( true );

			return false;
		},
		// navigate next event item in the currently selected event list.
		nextEvent: function( e ) {

			this.show();

			return false;
		},

		// show an event in the currently selected list of event items
		show: function( previous ) {
			// default = next
			previous = 'undefined' !== typeof previous ? previous : false;

			if ( null !== this.currEventIndex && 1 < this.currEvents.length ) {

				// unselect previous event.
				this.collection.at( this.currEvents[ this.currEventIndex ] ).set( 'selected', false );

				previous ? this.currEventIndex-- : this.currEventIndex++;

				// wrap boundary
				if ( 0 > this.currEventIndex ) {
					this.currEventIndex = this.currEvents.length-1;
				} else if ( this.currEvents.length <= this.currEventIndex ) {
					this.currEventIndex = 0;
				}

				this.collection.at( this.currEvents[ this.currEventIndex ] ).set( 'selected', true );
			}
		},

		filter: function( name, value ) {

			var toMatch = value.toLowerCase();

			// reset selection.
			if ( 0 < this.currEvents.length && null !== this.currEventIndex ) {
				this.toggleEvents( undefined );
			}

			this.collection.each( function( event ) {
				var propsToCheck = [], prop = null;

				if ( 'keyword' === name ) {
					prop = event.get( 'content' );
					prop && propsToCheck.push( prop.rendered );

					prop = event.get( 'title' );
					prop && propsToCheck.push( prop.rendered );
				} else {
					prop = event.get( name );
					prop && propsToCheck.push( prop );
				}

				event.set( 'hidden', true );

				for ( var i = 0, l = propsToCheck.length; i < l; i++ ) {
					if ( ~( propsToCheck[i].toLowerCase() ).indexOf( toMatch ) ) {
						event.set( 'hidden', false );
						break;
					}
				}
			});
		}

	});	

	var CalendarView = Bb.View.extend({
		tagName: 'div',

		className: 'simply-events-calendar',

		events: {
			'click .calendar-content__date-controls .prev-date' 				   : 'previous',
			'click .calendar-content__date-controls .next-date' 				   : 'next',
			'click .se-calendar-header .se-calendar-header__fullscreen'			   : 'fullscreen',
			'click .calendar-events-table .calendar-date[data-has-event="true"]'   : 'showEvents',
			'blur .date-selectors .year-selector, .date-selectors .month-selector' : 'gotoDateHandler',
			'blur .se-calendar-filter' 											   : 'filterEvents',
			'focusin .se-calendar-filter'										   : 'clearFilters'
		},

		initialize: function( options ) {
			this.currentDate       = null;
			this.selectedDay       = null;
			this.currFilter        = null;
			this.disableShowEvents = false;
			
			// setup smiple cache for housing calendar dates and events views.
			this.dateCache = new SimplyEventsCache( { expireTime: false } );
			this.eventsCache = new SimplyEventsCache();

			this.isFullscreen = false;

			this.options = options || {};

			if ( options.containerClass ) this.$el.addClass( options.containerClass );

			var that = this;
			throttleEvent( root, 'resize', function( e ) {
				if ( null !== this.currentDate && null !== this.selectedDay ) {
					var item = this.$el.find( 
						'td.calendar-date[year="' +this.currentDate[1]+ '"]' +
						'[month="' +(this.currentDate[0]+1)+ '"]' +
						'[day="' +this.selectedDay+ '"]'
					);

					if ( 1 === item.length ) {
						that.getEvents( this.currentDate[0], this.currentDate[1], function( eventsView ) {
							that.adjustEventsPosition( eventsView.el, item[0] );
						});
					}
				}

				return false;
			}.bind( this ) );

			$( 'html' ).on( 'click', function( e ) {
				e.stopPropagation();
				
				if ( null !== this.selectedDay ) {
					var tar = $( e.target );

					if ( !tar.hasClass( 'simply-events-calendar-events' ) && 0 === tar.parents( '.simply-events-calendar-events' ).length ) {
						this.getEvents( this.currentDate[0], this.currentDate[1], function( eventsView ) {
							eventsView.toggleEvents( undefined );
						});

						this.selectedDay = null;
					}
				}

			}.bind( this ) );
		},

		render: function() {
			// clean existing html contents.
			this.$el.empty();

			// render calendar header.
			var header = '<div class="se-calendar-header se-clearfix">';

			// render the title part if title is passed in the options object.
			if ( this.options.title ) {
				header += '<h3 class="se-calendar-header__title">' +this.options.title+ '</h3>';
			}

			header += '<div class="se-calendar-header__fullscreen" title="Fullscreen">\
						  <span class="dashicons dashicons-admin-page"></span>\
					   </div>';

			// render the calendar body which houses calendar dates and date controls.
			this.$content = $( '<div class="calendar-content"></div>' );

			this.$filters = $( 
				'<div class="calendar-filters-wrap">\
					<label class="se-label">Keyword<input type="text" class="se-calendar-filter se-input" data-filter="keyword" /></label>\
					<label class="se-label">Location<input type="text" class="se-calendar-filter se-input" data-filter="location" /></label>\
				</div>'
			);

			var selectors = $( '<div class="date-selectors"></div>' );

			this.$monthSelector = $( '<input type="text" class="month-selector se-input" placeholder="Month"></input>' ),
			this.$yearSelector  = $( '<input type="text" class="year-selector se-input" placeholder="Year"></input>' );

			selectors.append( this.$monthSelector ).append( this.$yearSelector );

			var dateControls = $( '<div class="calendar-content__date-controls"></div>' );

			dateControls.append( '<button type="button" class="date-control prev-date se-button" title="previous month">\
						       		 <span class="dashicons dashicons-arrow-left-alt2">\
					        	  </button>' )
						.append( selectors )
						.append( '<button type="button" class="date-control next-date se-button" title="next month">\
							   		 <span class="dashicons dashicons-arrow-right-alt2">\
					        	  </button>' );

			// create the date table header
			var tr = '<tr>',
				i  = 0,
				l  = days.length;

			for ( ; i < l; i++ ) tr += '<th><abbr title="' + days[i] + '">' + days[i].slice( 0, 3 ) + '</abbr></th>';
			tr += '</tr>';

			this.$table = $(
				'<div class="calendar-events-table-wrapper">\
					<table cellpadding="0" cellspacing="0" class="calendar-events-table">\
						<thead>' + tr + '</thead>\
						<tbody></tbody>\
					</table>\
				</div>'
			);

			var tableWrapper = $( '<div class="calendar-content__calendar-table-wrap"></div>' );
			tableWrapper.append( this.$filters ).append( dateControls ).append( this.$table );

			this.$content.append( tableWrapper );

			// here houses rendered events view.
			this.$eventsList = $( '<div class="calendar-events-views"></div>' );

			this.$content.append( this.$eventsList );

			// assemble now!
			this.$el.append( header ).append( this.$content );
			
			this.loadSpinner = new LoadSpinner( {
				element: this.$table[0],
				classes: 'simply-events-loadspinner',
				duration: 0,
				autoStart: false,
				overlay: true,
				text: 'Loading Events...'
			});

			var that = this;
			throttleEvent( this.el, 'scroll', function( e ) {
				if ( that.loadSpinner.isAnimating() ) that.loadSpinner.centerLoader()
			}, 100 );

			return this;
		},

		renderDate: function( month, year ) {

			var html 	  = '<tr>',
				now  	  = new Date(),
				isCurrent = false,
				d    	  = getDaysInMonth( month, year ),
				c    	  = 0,
				y    	  = 0,
				m    	  = 0;

			// invalid month
			if ( ! d ) return false;

			now = [ now.getFullYear(), now.getMonth(), now.getDate() ];

			// get the first day of the month
			// e.g. 0-6 (sun-sat)
			var _day = new Date( year, month, 1 ).getDay();

			// when the first day of the month is not a sunday.
			// then we should get some days from previous month
			// to compensate for the empty days.
			if ( 0 < _day ) {
				y = year, m = month - 1;
				// if previous month is lower than january we need to adjust to previous year.
				// and set month as december
				if ( 0 > m ) {
					y = year - 1;
					m += 12;
				}

				// days in previous month.
				var _d = getDaysInMonth( m, y );
				isCurrent = now[0] === y && now[1] === m;
				for ( var i = _d - ( _day - 1 ); c < _day; c++, i++ ) {
					html += '<td class="calendar-date' +( isCurrent && i === now[2] ? ' is-current' : '' )+ ' is-disabled" year="' +y+ '" month="' +( m + 1 )+ '" day="' +i+ '">' +i+ '</td>';
					// html += '<td class="calendar-date is-disabled" year="' + y + '" month="' + ( m + 1 ) + '" day="' + i + '">' + i + '</td>';
				}
			}

			// render the days for the selected month and year
			isCurrent = now[0] === year && now[1] === month;
			for ( var i = 1; i <= d; c++, i++ ) {
				if ( 0 < c && 0 === c % 7 ) html += '</tr><tr>';

				html += '<td class="calendar-date' +( isCurrent && now[2] === i ? ' is-current' : '' )+ '" year="' +year+ '" month="' +( month+1 )+ '" day="' +i+ '">' +i+ '</td>';
			}

			// same like before, we check if their are still room to fit days of the next month.
			// if the last day of the selected month is not a sunday
			_day = new Date( year, month + 1, 0 ).getDay();
			if ( 6 > _day ) {
				m = month + 1, y = year;
				if ( 11 < m ) {
					y = year + 1;
					m = 0;
				}

				isCurrent = now[0] === y && now[1] === m;
				for ( var i = 1; i <= ( 6 - _day ); i++ ) {
					html += '<td class="calendar-date' +( isCurrent && i === now[2] ? ' is-current' : '' )+ ' is-disabled" year="' +y+ '" month="' +( m + 1 )+ '" day="' +i+ '">' +i+ '</td>';
				}
			}

			html += '</tr>';

			return html;
		},

		updateHeader: function() {
			if ( null !== this.currentDate && 2 === this.currentDate.length ) {
				this.$monthSelector.val( months[ this.currentDate[0] ] );
				this.$yearSelector.val( this.currentDate[1] );
			}
		},

		gotoDate: function( month, year ) {
			// convert to number to be safe.
			month = month*1,
			year  = year*1;

			// invalid month index
			if ( ! months[ month ] ) return false;

			// check if the rendered dates is in the cache first.
			// cache key syntax: month-year
			var key = months[ month ] + '-' + year,
				dates = this.dateCache.get( key );

			if ( ! dates ) {
				dates = this.renderDate( month, year );
				this.dateCache.add( key, dates );
			}

			if ( null !== this.currentDate ) {
				// tell current events view to close any opened events
				this.getEvents( this.currentDate[0], this.currentDate[1], function( eventsView ) {
					eventsView.closeEvents();
				});

				var m = month * 1,
					y = year * 1;
				if ( m === this.currentDate[0] && y === this.currentDate[1] ) {
					this.updateHeader();
					return;
				}
			}

			// setup current date
			this.currentDate = [ month, year ];

			// rendering the new date rows.
			var tbody = this.$table.find( 'tbody' );

			if ( tbody ) {
				tbody.html( dates );

				// this.clearFilters();

				this.currFilter = null;

				// update dates in the calendar header.
				this.updateHeader();

				// render events for selected date.
				this.renderEvents( month, year );
			}

			return this;
		},

		getEvents: function( month, year, callback ) {
			var key = months[ month ] + '-' + year,
				eventsView = this.eventsCache.get( key ),
				callback = callback || function() {};

			if ( !eventsView ) {
				eventsView = new EventsView({
					collection: new Events,
					attributes: { year: year, month: month+1 }
				});
				this.eventsCache.add( key, eventsView );

				var that = this;
				eventsView.collection.fetch({
					data: { start_date: padNumber( year ) + '-' + padNumber( month + 1 ) },
					cache: true,
					success: function( collection, response, options ) {
						callback.apply( that, [eventsView] );
					}
				});
			} else {
				callback.apply( this, [eventsView] );
			}
		},

		getCurrentEvents: function( callback ) {
			this.disableShowEvents = false;

			if ( null === this.currentDate ) return false;

			// load spinner.
			this.loadSpinner.play();
			this.disableShowEvents = true;

			var cb = function( eventsView ) {
				var that = this;
				setTimeout( function() {
					that.loadSpinner.stop();
					that.disableShowEvents = false;
					callback.apply( that, [eventsView] );
				}, 250 );
			};

			this.getEvents( this.currentDate[0], this.currentDate[1], cb );
		},

		renderEvents: function( month, year ) {
			this.$eventsList.empty();

			var cb = function( eventsView ) {
				// clear the filters.
				this.clearFilters();

				this.$eventsList.append( eventsView.reRender().el );
				this.updateDateStatuses( eventsView.collection );
			}

			this.getCurrentEvents( cb );
		},

		previous: function() {
			if ( null === this.currentDate ) return false;

			var newMonth = this.currentDate[0] - 1,
				newYear  = this.currentDate[1];

			if ( 0 > newMonth ) {
				newMonth = 11;
				newYear -= 1;
			}

			this.gotoDate( newMonth, newYear );

		},

		next: function() {
			if ( null === this.currentDate ) return false;

			var newMonth = this.currentDate[0] + 1,
				newYear  = this.currentDate[1];

			if ( newMonth > 11 ) {
				newMonth = 0;
				newYear += 1;
			}

			this.gotoDate( newMonth, newYear );
		},

		/**
		 * Update the calendar dates status
		 * e.g. whether the date item has event and if so has the event passed or not
		 * 
		 * @param  Collection events collection of events
		 */
		updateDateStatuses: function( events ) {

			// clear all dates status.
			this.clearDateStatuses();

			var that   	  	= this,
				now    	  	= new Date().getTime(),
				addStatuses = function( item, time ) {
					var eventTime = new Date( 
							time.year, time.month-1,
							time.day, 'am' === time.meridiem ? time.hour : time.hour+12,
							time.minute
						),
						status = 'passed';

					if ( now < eventTime ) {
						status = 'not-passed';

						// event is equal or less than a week.
						if ( 7 >= ( eventTime.getTime() - now ) / ( 1000 * 60 * 60 * 24 ) ) status = 'near-pass';
					}

					item.attr( 'data-event-status', status );
					item.attr( 'data-has-event', true );
				},
				getDays = function( start, end ) {
					var d = end.day;

					if ( start.year > end.year || start.month > end.month ) return false;

					if ( end.year >= that.currentDate[1] && (end.month-1) > that.currentDate[0] ) return false;

					return (d - start.day) + 1;
				};

			events.each( function( event ) {
				var start  = event.get( 'start_datetime' ),
					end    = event.get( 'end_datetime' ),
					hidden = event.get( 'hidden' );

				var item = that.getDateItem( start.year, start.month, start.day );

				// if this event is hidden we should move along.
				if ( hidden ) return;

				var duration = getDays( start, end ),
					d        = start.day,
					i 		 = null;

				if ( false === duration ) return;

				while ( 0 < duration-- ) {
					start.day = d + duration;

					i = that.getDateItem( start.year, start.month, start.day );

					if ( i ) {
						addStatuses( i, start );
					}
				}
			});
		},

		clearDateStatuses: function() {
			this.$table.find( 'tbody td.calendar-date' ).each( function( i, el ) {
				el.setAttribute( 'data-event-status', '' );
				el.setAttribute( 'data-has-event', false );
			});
		},

		getDateItem: function( year, month, day ) {
			var attrFilterStr = '', namedArgsMap = [ 'year', 'month', 'day' ];

			for ( var i = 0, l = arguments.length; i < l; i++ ) {
				var arg = arguments[i];

				if ( 'undefined' !== typeof arg && _.isNumber( arg ) ) {
					attrFilterStr += '[' +namedArgsMap[i]+ '="' +arg+ '"]';
				}
			}
			
			var item = this.$content.find( 'td' + attrFilterStr );

			return 0 < item.length ? item : false;
		},

		showEvents: function( e ) {
			if ( this.disableShowEvents ) return false;
			
			var tar   = e.target,
				month = tar.getAttribute( 'month' )-1,
				year  = tar.getAttribute( 'year' ),
				that  = this;

			this.getEvents( month, year, function( eventsView ) {

				var day = tar.getAttribute( 'day' );

				eventsView.toggleEvents( day );

				that.$table.find( 'tbody td.calendar-date[day="' +this.selectedDay+ '"]' ).attr( 'data-selected', false );

				// only adjust the events view box if to the date element if we are not in fullscreen mode.
				if ( !this.isFullscreen ) that.adjustEventsPosition( eventsView.el, tar );

				that.selectedDay = day;

				tar.setAttribute( 'data-selected', true );
			});

			return false;
		},

		adjustEventsPosition: function( el, tar ) {
			var w = el.clientWidth,
				h = el.clientHeight,
				p = $( tar ).position(),
				e = 5,	// this is an extra space (in px) gap between el and target
				a = ['bottom', ''],	 // default position of el is below target.
				t = p.top + tar.offsetHeight + e,
				l = null;

			el.removeAttribute( 'data-position' );

			// can we fit el left of target?			
			if ( 0 <= ( p.left - ( w + e ) ) ) {
				a[0] = 'left';
				l = p.left - ( w + e );
			}
			// can we fit el right of target? 
			else if ( root.outerWidth >= ( p.left + tar.offsetWidth + w + e ) ) {
				a[0] = 'right';
				l = p.left + tar.offsetWidth + e;
			}

			// can we fit el top of target?
			else if ( 0 <= ( p.top - ( h + e ) ) ) {
				a[0] = 'top';
				t = p.top - ( h + e );
			}

			if ( 'left' === a[0] || 'right' === a[0] ) {
				t = h === tar.offsetHeight ? p.top : p.top - ( h - tar.offsetHeight ) / 2;

				if ( 0 <= t && 0 > ( p.top - h ) ) {
					a[1] = 'bottom';
					t = p.top + tar.offsetHeight - h;
				}

				if ( 0 > t ) {
					t = p.top;
					a[1] = 'top';
				}

			} else if ( 'top' === a[0] || 'bottom' === a[0] ) {
				l = w === tar.offsetWith ? p.left : p.left - ( w - tar.offsetWidth ) / 2;

				if ( 0 <= l && root.outerWidth < ( l + w ) ) {
					a[1] = 'right';
					l = p.left + tar.offsetWidth - w;
				}

				// if el is positioned offscreen to the left.
				// we should adjust to left of target.
				if ( 0 > l ) {
					l = p.left;
					a[1] = 'left';
				}
			}

			el.style.top = t + 'px';
			el.style.left = l + 'px';
			el.setAttribute( 'data-position', a[0] + ( !!a[1] ? '-' + a[1] : '' ) );

		},

		gotoDateHandler: function( e ) {
			var month = this.$monthSelector.val(),
				year  = this.$yearSelector.val();

			if ( false === this.validateMonth( month ) ) {
				month = this.currentDate[0] || new Date().getMonth();
			} else {
				if ( isNaN( month * 1 ) ) {
					month = this.getMonthIndex( month );
				} else {
					month = month * 1;
					month--;
				}
			}

			if ( false === this.validateYear( year ) ) {
				year = this.currentDate[1] || new Date().getFullYear();
				this.$yearSelector.val( year );
			}

			year = year*1;

			this.gotoDate( month , year );

			return false;
		},

		validateYear: function( year ) {
			year = year*1;

			// we only consider from year 1970 onwards to be valid year.
			return !( isNaN( year ) || 1970 > year );
		},

		validateMonth: function( month ) {
			var val = month * 1;

			if ( isNaN( val ) ) return -1 < this.getMonthIndex( month );

			return 0 < val && val <= 12;
		},

		getMonthIndex: function( month ) {
			// string of non valid numbers we then check if the string matches the name of the month.
			// we should ignore cases
			var name = month.toLowerCase(),
				m    = months;

			if ( 3 === name.length ) m = monthsShort;

			name = name.charAt(0).toUpperCase() + name.slice(1);

			return m.indexOf( name );
		},

		fullscreen: function() {
			this.isFullscreen = !this.isFullscreen;

			this.$el.toggleClass( 'fullscreen', this.isFullscreen );

			var $fullscreen = $( '.se-calendar-header__fullscreen' );

			if ( 1 === $fullscreen.length ) {
				$fullscreen.attr( 'title', this.isFullscreen ? 'Minimize' : 'Fullscreen' );
			}

			doc.body.classList.toggle( 'simply-events-calendar-fullscreen', this.isFullscreen );

			this.$el.css( 'padding', ( this.isFullscreen ? scrollbarWidth : 0 ) + 'px' );
		},

		filterEvents: function( e ) {
			var tar    = e.target,
				filter = tar.getAttribute( 'data-filter' ),
				empty  = _.isEmpty( tar.value );

			if ( null === this.currentDate || !filter ) return false;

			if ( null === this.currFilter && empty ) return false;

			if ( null !== this.currFilter && filter === this.currFilter[0] && tar.value === this.currFilter[1] ) return false;

			this.currFilter = empty ? null : [ filter, tar.value ];

			this.getCurrentEvents( function( eventsView ) {
				eventsView.filter( filter, tar.value );

				this.updateDateStatuses( eventsView.collection );
			});


			return false;
		},

		clearFilters: function( e ) {

			this.$filters.find( '.se-calendar-filter' ).val( '' );

			// clear all event hidden status
			if ( null !== this.currentDate ) {
				this.getEvents( this.currentDate[0], this.currentDate[1], function( eventsView ) {
					eventsView.collection.each( function( event ) {
						event.set( 'hidden', false );
					});
				});
			}

			return false;
		}
	});

	/**
	 * options:
	 *
	 * title: 
	 * 
	 * class:
	 *
	 * container: 
	 *
	 * showWeekNumber: false
	 *
	 * showPreviousMonth: true
	 * ..
	 */
	var API = function( options ) {

		var self = this,
			calendar = null;

		var init = function() {

			// check options.
			var calendarOptions = {};

			if ( _.has( options, 'containerClass' ) && ! _.isEmpty( options.containerClass ) ) {
				calendarOptions.containerClass = options.containerClass;
			}

			if ( _.has( options, 'title' ) && ! _.isEmpty( options.title ) ) {
				calendarOptions.title = options.title;
			}

			calendar = new CalendarView( calendarOptions );

			if ( _.has( options, 'container' ) ) {
				
				var $container = $( options.container );

				if ( 0 === $container.length ) {
					throw new Error( 'specified container not found.' );
				}

				$container.append( calendar.render().el );
			}

			self.today();
		};

		self.gotoDate = function( month, year ) {
			calendar.gotoDate( month, year );
		};

		self.nextMonth = function() {
			calendar.next();
		};

		self.previousMonth = function() {
			calendar.previous();
		};

		self.nextYear = function( sameMonth ) {
			sameMonth = sameMonth || false;

			var current = calendar.currentDate;
			self.gotoDate( sameMonth ? current[0] : 0, ++current[1] );
		}

		self.previousYear = function( sameMonth ) {
			sameMonth = sameMonth || false;

			var current = calendar.currentDate;
			self.gotoDate( sameMonth ? current[0] : 0, --current[1] );
		};

		self.currentDate = function() {
			return null !== calendar.currentDate
						? padNumber( calendar.currentDate[0]+1 ) + '-' + padNumber( calendar.currentDate[1] )
						: false;
		};

		self.today = function() {
			var now = new Date(),
				m   = now.getMonth(),
				y   = now.getFullYear();

			if ( null !== calendar.currentDate && m === calendar.currentDate[0] && y === calendar.currentDate[1] ) {
				return false;
			}

			self.gotoDate( m, y );
		};

		init();

		return self;
	};

	window.SimplyEvents = new API( { 
		containerClass: _data.container_class,
		title: _data.title,
		container: '#' + _data.id
	} );

})( window, document, jQuery, Backbone );