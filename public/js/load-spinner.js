/**
 * Polyfills for ClassList
 * @source https://gist.github.com/k-gun/c2ea7c49edf7b757fe9561ba37cb19ca
 */
;(function() {
    // helpers
    var regExp = function(name) {
        return new RegExp('(^| )'+ name +'( |$)');
    };
    var forEach = function(list, fn, scope) {
        for (var i = 0; i < list.length; i++) {
            fn.call(scope, list[i]);
        }
    };

    // class list object with basic methods
    function ClassList(element) {
        this.element = element;
    }

    ClassList.prototype = {
        add: function() {
            forEach(arguments, function(name) {
                if (!this.contains(name)) {
                    this.element.className += ' '+ name;
                }
            }, this);
        },
        remove: function() {
            forEach(arguments, function(name) {
                this.element.className =
                    this.element.className.replace(regExp(name), '');
            }, this);
        },
        toggle: function(name) {
            return this.contains(name) 
                ? (this.remove(name), false) : (this.add(name), true);
        },
        contains: function(name) {
            return regExp(name).test(this.element.className);
        },
        // bonus..
        replace: function(oldName, newName) {
            this.remove(oldName), this.add(newName);
        }
    };

    // IE8/9, Safari
    if (!('classList' in Element.prototype)) {
        Object.defineProperty(Element.prototype, 'classList', {
            get: function() {
                return new ClassList(this);
            }
        });
    }

    // replace() support for others
    if (window.DOMTokenList && DOMTokenList.prototype.replace == null) {
        DOMTokenList.prototype.replace = ClassList.prototype.replace;
    }
})();

;( function( root, doc, undefined ) {

	'use strict';

	var throttleEvent = function( obj, type, callback, delay ) {
		obj = obj || window;
		delay = delay || 250;

		var running = false,
			func    = function( e ) {
				if ( running )
					return;

				running = true;
				setTimeout( function() {
					requestAnimationFrame( function() {
						callback.apply( obj, [e] );

						running = false;
					} );
				}, delay );
			};

		obj.addEventListener( type, func );
	};

	var LoadSpinner = function( opts ) {

		var animating   = false,
			settings    = {},
			loadWrapper = null,
			overlay     = null,
			self        = this;

		var timer = null;

		var init = function() {
			opts = 'undefined' !== typeof opts ? opts : {};

			var defaults = {
				// duration unit in milliseconds. If duration is set to false or 0 <= then will animate
				// indefinitely until manually calling .stop()
				duration: 3000,
				delay: 0,
				animation: 'default',
				text: '',
				element: null,
				classes: '',
				overlay: false,
				autoStart: true,
				callback: function() {}
			};

			for ( var prop in defaults ) {
				if ( opts.hasOwnProperty( prop ) ) {
					defaults[ prop ] = opts[ prop ];
				}
			}

			settings = defaults;

			if ( !!settings.element ) {
				if ( 'string' === typeof settings.element ) {
					settings.element = document.querySelector( settings.element );
				}
			}

			// is this really an element?
			if ( null === settings.element || !settings.element instanceof Element ) {
				// either the bounded element was not found or was an invalid object.
				// we should use <body> element instead.
				settings.element = document.body;
			}

			// render the loader
			render();

			// initially play the spinner.
			settings.autoStart && self.play();
		};

		var render = function() {

			if ( null !== loadWrapper ) {
				// removing all children of loadWrapper
				while ( loadWrapper.firstChild ) loadWrapper.removeChild( loadWrapper.firstChild );

				overlay = null;
			} else {
				loadWrapper = doc.createElement( 'div' );
				// concatenate strings of classes and trim any leading or ending spaces.
				loadWrapper.className = ( 'sls-wrapper ' + settings.classes ).replace( /^\s+|\s+$/gm, '' );
			}

			var animateWrapper = doc.createElement( 'div' );
			animateWrapper.className = 'sls-animate-wrapper';

			for ( var i = 1, l = 4; i <= l; i++ ) {
				var loaderDot = doc.createElement( 'div' );
				loaderDot.className = 'sls-loader-dot loader-dot-' +i;

				animateWrapper.appendChild( loaderDot );
			}

			loadWrapper.appendChild( animateWrapper );

			// add load spinner text
			var text = doc.createElement( 'div' );
			text.className = 'sls-loader-text';
			text.innerText = settings.text;

			loadWrapper.appendChild( text );

			if ( settings.overlay ) {
				overlay = doc.createElement( 'div' );
				overlay.className = 'sls-overlay';
				doc.body.appendChild( overlay );
			}

			doc.body.appendChild( loadWrapper );

			// update the loader position upon window resize.
			throttleEvent( root, 'resize', function( e ) {
				if ( animating ) self.centerLoader( settings.element );
			}, 100 );

		};

		var getPosition = function( elem ) {

			if ( !elem ) return false;

			var pos = { top: 0, left: 0 };

			// support for getBoundingClientRect?
			if ( elem.getBoundingClientRect ) {
				// elem position relative to viewport (ie. scroll can affect these values)
				var rect       = elem.getBoundingClientRect(),
					scrollLeft = root.pageXOffset || doc.documentElement.scrollLeft,
					scrollTop  = root.pageYOffset || doc.documentElement.scrollTop;
				
				pos.top = rect.top + scrollTop;
				pos.left = rect.left + scrollLeft;				
			} else {
				do {
					pos.top += elem.offsetTop;
					pos.left += elem.offsetLeft;
				} while ( elem = elem.offsetParent )
			}

			return pos;
		}

		var hasOverlay = function() {
			return settings.overlay && null !== overlay && overlay instanceof Element;
		}

		self.centerLoader = function( elem ) {
			elem = elem || settings.element;

			var w = elem.offsetWidth,
				h = elem.offsetHeight,
				p = getPosition( elem );

			// center load wrapper to the target elements position
			loadWrapper.style.top = p.top + (h/2) - (loadWrapper.offsetHeight/2) + 'px';
			loadWrapper.style.left = p.left + (w/2) - (loadWrapper.offsetWidth/2) + 'px';

			if ( hasOverlay() ) {
				overlay.style.width = w + 'px';
				overlay.style.height = h + 'px';
				overlay.style.top = p.top + 'px';
				overlay.style.left = p.left + 'px';
			}
		};

		self.play = function( duration, delay, callback ) {
			if ( timer ) return false;
			
			duration = duration*1 | settings.duration;
			delay    = delay*1 | settings.delay;
			callback = callback || settings.callback;

			if ( hasOverlay() ) overlay.classList.remove( 'hide' );

			timer = setTimeout( function() {
				loadWrapper.classList.add( 'sls-animate' );
				self.centerLoader();

				if ( 0 < duration ) {
					setTimeout( function() {
						callback.call( self, loadWrapper );

						self._stop();
					}, duration );
				}

			}, delay );
		};

		self.stop = function() {
			if ( null === timer ) return false;

			clearTimeout( timer );

			timer = null;
			
			if ( hasOverlay() ) overlay.classList.add( 'hide' );

			loadWrapper.classList.remove( 'sls-animate' );
		}


		self.destroy = function() {
			animating = false;
			loadWrapper = null;
		}

		self.isAnimating = function() {
			return null !== timer;
		}

		init();

		return self;
	}

	root.LoadSpinner = LoadSpinner;

} )( window, document );