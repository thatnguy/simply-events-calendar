# README #

A WordPress events calendar list plugin.

### requirements ###
* BackboneJS
* jQuery
* UnderscoreJS

All the requirements above should already come with WordPress.

### Why would you use this plugin? ###

* Only want a simple way of listing events in a calendar widget
* Want a lightweight events listing
* Want to have recurring events

### Supports ###
* Recurring Events - allows setup of a repeating event. Recurring events are like normal events that is stored as a single event in the server. Instances of this event is calculated on request and created based on the event setup options e.g. repeating every 2 days until 5 occurrences.
* Google Maps for event location - uses Google Maps API for address input type-ahead.
It is required that you provide your Google Map API KEY in the options panel.
Read [here](https://developers.google.com/maps/documentation/javascript/get-api-key)
to find out how to get one.


### Warnings ###

You should not use this plugin expecting to have attendance or/and ticket tracking as for now this is only a simple events list.


### License ###
[MIT](https://opensource.org/licenses/MIT)

Copyright (c) 2017 - present, Phong Nguyen