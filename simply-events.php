<?php
/**
 * The plugin bootstrap file
 *
 * Plugin Name:       Simply Events
 * Plugin URI:        https://bitbucket.org/thatnguy/simplyevents
 * Description:       A simple event calendar.
 * Version:           1.0.0
 * Author:            phong
 * Author URI:        https://bitbucket.org/thatnguy
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       simply-events
 * Domain Path:       /languages
 */

/**
 * @note a pretty permalink should be used for WP-API to function. do not use the first permalink settings in admin i.e ?p=
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

function simply_events_activate() {
	// requirements check.

	if ( !is_plugin_active( 'rest-api/plugin.php' ) ) {

		deactivate_plugins( plugin_basename( __FILE__ ) );

		wp_die( 
				sprintf( __( 'This plugin requires WP-API plugin. Check it out %s', 'simply-events' ),
					'<a href="https://wordpress.org/plugins/json-rest-api/">here</a>'
				),
				'Require WP API Plugin',
				array( 'back_link' => true )
		);
	}

}
register_activation_hook( __FILE__, 'simply_events_activate' );

require_once plugin_dir_path( __FILE__ ) . 'inc/simply-events.class.php';

// Begin the execution of plugin.
$plugin = new Simply_Events();
