<?php
/**
 * Widget Class
 * 
 * @package Simply Events
 */
class Simply_Events_Widget Extends WP_Widget {

	// register our widget
	public function __construct() {

		load_plugin_textdomain( 'simply-events' );

		parent::__construct(
			'simply_events_calender',
			__( 'Simply Events Calendar', 'simply-events' ),
			array(
				'description' => __( 'Display Events Calendar', 'simply-events' ),
			)
		);

	}

	public function enqueue_script() {

		$script_uri = plugin_dir_url( dirname( __FILE__ ) ) . 'public/js';

		// main widget script.
		wp_enqueue_script( 'simply-events-calendar', "{$script_uri}/widget.js", array( 'backbone' ), '', true );
	}

	public function localize_script() {
		$settings = $this->get_settings();

		$data = array(
			'url'   => site_url() . '/wp-json/simply-events-api/simply-events',	//@todo make this into a filter instead of hard copy here.
			'nonce' => wp_create_nonce( 'wp_rest' ),
			'widget_id' => $this->id,	// the widget instance id should be available by now.
			// widget calendar options.
			'container_class' => '',
			'title' => isset( $settings[ $this->number ]['title'] ) ? $settings[ $this->number ]['title'] : '',
			// add more...
		);

		$data = apply_filters( 'simply_events_calendar_data', $data );

		wp_localize_script( 'simply-events-calendar', 'simply_events_data', $data );
	}

	/**
	 * Front-end display of the widget.
	 *
	 * @param  Array $args     arguments passed to register_sidebar
	 * @param  Array $instance widget instance option values
	 */
	public function widget( $args, $instance ) {

		echo $args['before_widget'];
		echo $args['after_widget'];

		$this->enqueue_script();
		$this->localize_script();
	}

	/**
	 * Process widget options on save. Use this to update widget option.
	 * 
	 * @param  Array $new_instance values ready to be saved
	 * @param  Array $old_instance previously saved values.
	 * @return Array updated values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		return $instance;
	}

	/**
	 * Define the back-end widget form.
	 * 
	 * @see  widgets panel in dashboard
	 * @param  Array $instance previous saved option values
	 * @return [type]           [description]
	 */
	public function form( $instance ) {
		$title = isset( $instance['title'] ) ? $instance['title'] : '';
		$title_field_id = $this->get_field_id( 'title' );
		$title_field_name = $this->get_field_name( 'title' );

		?>
		<p>
			<label for="<?php echo $title_field_id; ?>"><?php esc_html_e( 'Title:', 'simply-events' ); ?></label>
			<input class="widefat" id="<?php echo $title_field_id; ?>" name="<?php echo $title_field_name; ?>" type="text" value="<?php echo $title; ?>" />
		</p>
		<?php
	}
}