<?php
/**
 * Plugin Core class file.
 *
 * @package  Simply Events
 * @since    1.0.0
 */

class Simply_Events {

	/**
	 * The unique identifier of this plugin.
	 * 
	 * @var string 
	 */
	protected $plugin_name;


	/**
	 * current version of the plugin.
	 * 
	 * @var string
	 */
	protected $version;

	private $dir_path;

	private $dir_uri;

	public function __construct() {
		$this->plugin_name = 'simply-events';
		$this->version = '1.0.0';

		// setup plugin path.
		$this->dir_path = apply_filters( 'simply_events_path', plugin_dir_path( dirname( __FILE__ ) ) );
		$this->dir_uri = apply_filters( 'simply_events_uri', plugin_dir_url( dirname( __FILE__ ) ) );

		$this->setup_mysql_functions();
		$this->setup_locale();
		$this->setup_admin_hooks();
		$this->setup_public_hooks();
		$this->setup_widgets();
	}

	private function setup_locale() {
		add_action( 'plugins_loaded', array( $this, 'load_plugin_textdomain' ) );
	}


	private function setup_admin_hooks() {
		require_once $this->dir_path . '/admin/simply-events-admin.class.php';

		$plugin_admin = new Simply_Events_Admin( $this->plugin_name, $this->version );

		add_action( 'admin_init', array( $plugin_admin, 'admin_init' ) );

		add_action( 'admin_enqueue_scripts', array( $plugin_admin, 'enqueue_styles' ) );
		// add_action( 'admin_enqueue_scripts', array( $plugin_admin, 'enqueue_scripts' ) );

		// register our post type.
		add_action( 'init', array( $plugin_admin, 'register_event_post_type' ) );

		// add custom columns for custom post type.
		add_filter( "manage_{$this->plugin_name}_posts_columns", array( $plugin_admin, 'manage_event_columns' ) );
		add_action(	"manage_{$this->plugin_name}_posts_custom_column", array( $plugin_admin, 'render_event_column' ), 10, 2 );
		// make columns sortable.
		add_filter( "manage_edit-{$this->plugin_name}_sortable_columns", array( $plugin_admin, 'event_sortable_columns' ) );
		add_action( 'load-edit.php', array( $plugin_admin, 'admin_edit_screen_load' ) );

		// Setup meta boxes and enqueue admin scripts when loading the post or creating a post screen.
		add_action( 'load-post.php', array( $plugin_admin, 'setup_metaboxes' ) );
		add_action( 'load-post-new.php', array( $plugin_admin, 'setup_metaboxes' ) );
		add_action( 'load-post.php', array( $plugin_admin, 'enqueue_scripts' ) );
		add_action( 'load-post-new.php', array( $plugin_admin, 'enqueue_scripts' ) );

		// async scripts.
		add_filter( 'script_loader_tag', array( $plugin_admin, 'add_async_defer_scripts' ), 10, 2 );

		// add options page for our plugin.
		add_action( 'admin_menu', array( $plugin_admin, 'settings_page' ) );
		
		// filters in the post table.
		add_action( 'restrict_manage_posts', array( $plugin_admin, 'add_event_list_filters' ), 10, 2 );
	}

	private function setup_public_hooks() {
		require_once $this->dir_path . '/public/simply-events-public.class.php';

		$plugin_public = new Simply_Events_Public( $this->plugin_name, $this->version );

		add_action( 'wp_enqueue_scripts', array( $plugin_public, 'enqueue_styles' ) );
		add_action( 'wp_enqueue_scripts', array( $plugin_public, 'enqueue_scripts' ) );

		add_action( 'rest_api_init', array( $plugin_public, 'register_routes' ) );
		
		// add meta data to REST response
		add_action( 'rest_api_init', array( $plugin_public, 'rest_add_meta_fields' ) );

		// allow meta_query to be part of the valid query list in REST
		add_action( 'rest_query_vars', array( $plugin_public, 'allow_meta_query' ) );

		// filter meta_query
		add_filter( "rest_prepare_{$this->plugin_name}", array( $plugin_public, 'rest_prepare_response' ), 10, 3 );
		
		add_filter( 'rest_query_vars', array( $plugin_public, 'add_rest_query_vars' ) );
		add_filter( 'posts_where', array( $plugin_public, 'events_where_clause' ), 10, 2 );
	}

	private function setup_widgets() {
		add_action( 'widgets_init', array( $this, 'register_widgets' ) );
	}

	/**
	 * Setup MySQL server functions for handy use in our plugin.
	 */
	private function setup_mysql_functions() {
		global $wpdb;

		// first delete existing function
		$wpdb->query( 'DROP FUNCTION se_check_in_month' );

		$wpdb->query(
			"CREATE FUNCTION se_check_in_month(s BIGINT(20), ds BIGINT(20), de BIGINT(20), i INT(10) ) RETURNS TINYINT(1)
			BEGIN
				DECLARE d DATETIME;
				SET d = FROM_UNIXTIME(s);
				WHILE s <= de DO
					IF s >= ds THEN
						return 1;
					END IF;

					SET d = DATE_ADD(d, INTERVAL i MONTH);
					SET s = UNIX_TIMESTAMP(d);
				END WHILE;

				RETURN 0;
			END;"
		);
	}

	/**
	 * register calender widget.
	 */
	public function register_widgets() {
		require_once $this->dir_path . 'inc/simply-events-widget.class.php';

		register_widget( 'Simply_Events_Widget' );
	}

	/**
	 * Load the plugin text domain for translation.
	 */
	public function load_plugin_textdomain() {
		load_plugin_textdomain(
			'simply-events',
			false,
			$this->dir_path . '/languages'
		);
	}

	/**
	 * retrieve the plugin version number.
	 * 
	 * @return string version number
	 */
	public function get_version() {
		return $this->version;
	}

	/**
	 * retrieve the plugin name.
	 * 
	 * @return string plugin name
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}
}

